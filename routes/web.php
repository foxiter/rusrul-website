<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index');

# раздел новостей на сайте #
Route::group(['prefix' => 'news', 'as' => 'news.'], function() {
    Route::get('/', 'FrontController@showNewsList');
    Route::get('{code}', 'FrontController@showNewsItem');
});

# раздел для ПК-игры и редактора #
Route::group(['prefix' => 'pc-game', 'as' => 'pc.'], function() {
    Route::get('/', 'FrontController@viewPcGameIndex')->name('index');
    Route::get('v{version}', 'FrontController@viewPcGameRelease')->name('release');

    Route::group(['prefix' => 'editor', 'as' => 'editor.'], function() {
        Route::get('v{version}', 'FrontController@viewQuestionsEditorRelease')->name('release');
    });
});

# руты для онлайн-игры
Route::group(['prefix' => 'online', 'as' => 'online.'], function() {
    Route::get('/', 'FrontController@onlineGameMainPage')->name('index');

    # а здесь будет описано всё то, что нужно будет для нормального функционирования игры
    ## OAUTH-авторизация
    Route::get('auth', 'OAuth\StartAuthorization@run');
    Route::get('register', 'OAuth\VerifyAuthorization@step1');

    ## переход к игре
    Route::group(['prefix' => 'game'], function() {
        Route::get('/', 'OnlineGameController@index')->name('app');

        Route::group(['prefix' => 'api', 'as' => 'api.'], function() {
            Route::post('get_player', 'OnlineGameController@getPlayer')->name('player');
            Route::post('complete_game', 'OnlineGameController@saveResults')->name('complete');
            Route::post('ping', 'OnlineGameController@makeAction')->name('ping');
            Route::post('top', 'OnlineGameController@getTopPlayers')->name('top');
        });
    });
});

# Admin panel routes | Registration is not available #
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['prefix' => 'adm', 'as' => 'admin.'], function() {
    Route::get('/first', 'AdminController@index')->name('dashboard');

    Route::group(['prefix' => 'news', 'as' => 'news.'], function() {
        Route::get('add', 'AdminController@addNewsItem')->name('add');
        Route::post('add', 'AdminController@saveNewsItem');
    });

    Route::group(['prefix' => 'pages', 'as' => 'pages.'], function() {
        Route::get('add', 'AdminController@addNewPage')->name('add');
        Route::post('add', 'AdminController@saveNewPage');
    });

    Route::group(['prefix' => 'pc_releases', 'as' => 'pc_game.'], function () {
        Route::get('/', 'AdminController@listGameReleases')->name('list');
        Route::get('new', 'AdminController@newGameRelease')->name('new_release');
        Route::post('new', 'AdminController@createGameRelease');
    });

    Route::group(['prefix' => 'editor_releases', 'as' => 'qeditor.'], function() {
        Route::get('/', 'AdminController@listEditorReleases')->name('list');
        Route::get('new', 'AdminController@newEditorRelease')->name('new_release');
        Route::post('new', 'AdminController@createEditorRelease');
    });

    Route::group(['prefix' => 'online_stats', 'as' => 'online.'], function() {
        Route::get('/', 'AdminController@onlineGameStatsMain')->name('main');
    });

    Route::get('/settings', 'AdminController@getSettingsPage')->name('settings');
    Route::post('/settings', 'AdminController@saveSettingsPage');
});

# для страниц сайта на фронте #
Route::get('{slug}', 'FrontController@getPage');