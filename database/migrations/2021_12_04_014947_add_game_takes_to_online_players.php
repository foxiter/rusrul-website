<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGameTakesToOnlinePlayers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('online_players', function (Blueprint $table) {
            $table->integer('game_takes')->default(100)->after('games_played');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('online_players', function (Blueprint $table) {
            $table->dropColumn('game_takes');
        });
    }
}
