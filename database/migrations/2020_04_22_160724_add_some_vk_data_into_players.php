<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeVkDataIntoPlayers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('online_players', function (Blueprint $table) {
            $table->string('vk_avatar', 255)->nullable()->after('vk_id');
            $table->integer('vk_sex')->nullable()->after('vk_avatar');
            $table->string('vk_screen_name', 255)->nullable()->after('vk_sex');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('online_players', function (Blueprint $table) {
            $table->dropColumn('vk_avatar');
            $table->dropColumn('vk_sex');
            $table->dropColumn('vk_screen_name');
        });
    }
}
