<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnlinePlayerStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_player_stats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('player_id');
            $table->boolean('win')->default(0);
            $table->boolean('lose')->default(0);
            $table->integer('total_win')->default(0);
            $table->integer('total_lose')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_player_stats');
    }
}
