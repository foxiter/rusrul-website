<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnlinePlayerTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_player_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('player_id')->nullable();
            $table->string('operation_id', 255);
            $table->string('type', 32);
            $table->double('amount', 10, 2);
            $table->double('received', 10, 2);
            $table->integer('takes_added');
            $table->double('raised_fund_amount', 10, 2);
            $table->string('label', 255)->nullable();
            $table->string('operation_label', 255)->nullable();
            $table->timestamp('payed_timestamp')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_player_transactions');
    }
}
