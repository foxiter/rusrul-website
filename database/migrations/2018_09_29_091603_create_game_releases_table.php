<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameReleasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_releases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('major_version')->default(0);
            $table->integer('minor_version')->default(0);
            $table->string('version');
            $table->dateTime('release_date');
            $table->longText('description');
            $table->string('download_link')->nullable();
            $table->integer('filesize')->default(0);
            $table->boolean('is_beta')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_releases');
    }
}
