<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsitePage extends Model
{
    protected $table = 'website_pages';
    protected $primaryKey = 'id';
}
