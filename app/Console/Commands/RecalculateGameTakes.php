<?php

namespace App\Console\Commands;

use App\OnlinePlayer;
use App\OnlinePlayerStat;
use Illuminate\Console\Command;

class RecalculateGameTakes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'online:recalculate_takes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        # get all online players
        $allPlayers = OnlinePlayer::query()->get();
        foreach ($allPlayers as $allPlayer) {
            # get count of takes from 21/12/04 00:00 MSK
            $takesCount = OnlinePlayerStat::wherePlayerId($allPlayer->id)->where('created_at', '>=', '2021-12-04 00:00:00')->count();
            $takesRemain = $allPlayer->game_takes - $takesCount;
            $this->info('Player ' . $allPlayer->first_name . ' ' . $allPlayer->last_name . ' --- takes: ' . $takesCount . ' ; remain: ' . $takesRemain);

            $allPlayer->game_takes = $takesRemain;
            $allPlayer->save();
        }

        $this->warn('All takes recalculated successfully.');
        return 0;
    }
}
