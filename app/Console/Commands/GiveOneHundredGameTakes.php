<?php

namespace App\Console\Commands;

use App\OnlinePlayer;
use Illuminate\Console\Command;

class GiveOneHundredGameTakes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'online:give_hundred_tasks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        # get all online players
        $allPlayers = OnlinePlayer::query()->get();
        foreach ($allPlayers as $allPlayer) {
            # give them 100 takes
            $takesRemain = $allPlayer->game_takes + 150;
            $allPlayer->game_takes = $takesRemain;
            $allPlayer->timestamps = false;
            $allPlayer->save();
        }

        $this->warn('All takes awarded successfully.');
        return 0;
    }
}
