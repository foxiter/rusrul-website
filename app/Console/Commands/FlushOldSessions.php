<?php

namespace App\Console\Commands;

use App\OnlinePlayerSession;
use Carbon\Carbon;
use Illuminate\Console\Command;

class FlushOldSessions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'online:flush_sessions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $oldSessions = OnlinePlayerSession::where('valid_through', '<', Carbon::now('Europe/Moscow')->toDateTimeString())->get();

        foreach ($oldSessions as $oldSession) {
            $oldSession->delete();
        }

        return true;
    }
}
