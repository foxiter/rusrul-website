<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnlinePlayer extends Model
{
    public function gameSessions() {
        return $this->hasMany('App\OnlinePlayerSession', 'player_id');
    }

    public function gameResults() {
        return $this->hasMany('App\OnlinePlayerStat', 'player_id');
    }

    public function gameWins() {
        return $this->gameResults()->where('total_win', '=', 1180000)->count();
    }
}
