<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameRelease extends Model
{
    protected $table = 'game_releases';
    protected $primaryKey = 'id';
}
