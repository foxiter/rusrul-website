<?php

function convertDateToHuman($date) {
    $dateItems = explode(' ', $date);
    $dateFull = $dateItems[0];

    $dateFullSegments = explode('-', $dateFull);

    $months = [
        1 => 'января',
        2 => 'февраля',
        3 => 'марта',
        4 => 'апреля',
        5 => 'мая',
        6 => 'июня',
        7 => 'июля',
        8 => 'августа',
        9 => 'сентября',
        10 => 'октября',
        11 => 'ноября',
        12 => 'декабря',
    ];

    return intval($dateFullSegments[2]) . ' ' . $months[intval($dateFullSegments[1])] . ' ' . $dateFullSegments[0];
}