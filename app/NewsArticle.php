<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsArticle extends Model
{
    protected $table = 'news_articles';
    protected $primaryKey = 'id';

    public function author() {
        return $this->belongsTo('App\User', 'user_created');
    }

    public function editor() {
        return $this->belongsTo('App\User', 'user_updated');
    }
}
