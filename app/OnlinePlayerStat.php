<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnlinePlayerStat extends Model
{
    public function player() {
        return $this->belongsTo('App\OnlinePlayer', 'player_id');
    }
}
