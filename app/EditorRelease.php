<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EditorRelease extends Model
{
    protected $table = 'editor_releases';
    protected $primaryKey = 'id';
}
