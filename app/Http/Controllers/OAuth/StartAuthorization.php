<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 08.01.2017
 * Time: 16:48
 */

namespace App\Http\Controllers\OAuth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class StartAuthorization extends Controller
{
    public function run()
    {
        if (!isset($_GET['social'])) {
            return 'No social network selected';
        }

        if (!isset($_GET['redirect_uri'])) {
            return 'No host selected';
        }

        if ($socialNetwork = $_GET['social']) {
            switch ($socialNetwork) {
                /*case 'fb': {
                    $fb_client_id = '1281258195270851';
                    $fb_redirect_to = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . '/register/';
                    $fb_url = 'https://www.facebook.com/v2.8/dialog/oauth';

                    $fb_url_params = array(
                        'client_id'     => $fb_client_id,
                        'redirect_uri'  => $fb_redirect_to,
                        'response_type' => 'code',
                        'scope'         => 'public_profile,email,user_friends',
                        'display'       => 'popup'
                    );

                    session_start();
                    $_SESSION['social'] = 'fb';
                    session(['redirect_uri' => $_GET['redirect_uri']]);

                    # добавляем ещё кое-что в сессию на тот случай, если вдруг авторизация идёт из личного кабинета
                    if (!empty($_GET['passport'])) {
                        session(['givenPassportID' => $_GET['passport']]);
                    }

                    return redirect($fb_url . '?' . urldecode(http_build_query($fb_url_params)));
                }
                break;
                case 'google': {
                    $gp_client_id = '325658553609-f6a9hq9ejbd1d3cvu7pt8m3o768bv3da.apps.googleusercontent.com';
                    $gp_redirect_to = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . '/register/';
                    $gp_auth_url = 'https://accounts.google.com/o/oauth2/auth';

                    $gp_auth_params = [
                        'redirect_uri' => $gp_redirect_to,
                        'response_type' => 'code',
                        'client_id' => $gp_client_id,
                        'scope' => 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'
                    ];

                    session_start();
                    $_SESSION['social'] = 'google';
                    session(['redirect_uri' => $_GET['redirect_uri']]);

                    # добавляем ещё кое-что в сессию на тот случай, если вдруг авторизация идёт из личного кабинета
                    if (!empty($_GET['passport'])) {
                        session(['givenPassportID' => $_GET['passport']]);
                    }

                    return redirect($gp_auth_url . '?' . urldecode(http_build_query($gp_auth_params)));
                }
                break;
                case 'twitter': {
                    $twi_consumer_key = 'PVoFAWgo44FhFt8ClUVDptyKq';
                    $twi_consumer_secret = 'bXu3B9vBdtFyb65Je8FG4gWwFpmFX1x2TMDoXwPGRsaEblZM3i';

                    $twi_request_token_url = 'https://api.twitter.com/oauth/request_token';
                    $twi_authorize_url = 'https://api.twitter.com/oauth/authorize';
                    $twi_callback_url = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . '/register/';

                    $oauth_nonce = md5(uniqid(rand(), true));
                    $oauth_timestamp = time();

                    $twi_parameters = [
                        'oauth_callback=' . urlencode($twi_callback_url) . '&',
                        'oauth_consumer_key=' . $twi_consumer_key . '&',
                        'oauth_nonce=' . $oauth_nonce . '&',
                        'oauth_signature_method=HMAC-SHA1' . '&',
                        'oauth_timestamp=' . $oauth_timestamp . '&',
                        'oauth_version=1.0'
                    ];

                    $oauth_base_text = implode('', array_map('urlencode', $twi_parameters));
                    $key = $twi_consumer_secret . '&';
                    $oauth_base_text = 'GET' . '&' . urlencode($twi_request_token_url) . '&' . $oauth_base_text;
                    $oauth_signature = base64_encode(hash_hmac('sha1', $oauth_base_text, $key, true));

                    $twi_request_params = [
                        '&' . 'oauth_consumer_key=' . $twi_consumer_key,
                        'oauth_nonce=' . $oauth_nonce,
                        'oauth_signature=' . urlencode($oauth_signature),
                        'oauth_signature_method=HMAC-SHA1',
                        'oauth_timestamp=' . $oauth_timestamp,
                        'oauth_version=1.0'
                    ];

                    $twi_url = $twi_request_token_url . '?oauth_callback=' . urlencode($twi_callback_url) . implode('&', $twi_request_params);

                    $twi_request_response = file_get_contents($twi_url);
                    parse_str($twi_request_response, $twi_request_result);

                    $twi_oauth_token = $twi_request_result['oauth_token'];
                    $twi_oauth_token_secret = $twi_request_result['oauth_token_secret'];

                    session_start();
                    $_SESSION['social'] = 'twitter';
                    session(['redirect_uri' => $_GET['redirect_uri']]);

                    # добавляем ещё кое-что в сессию на тот случай, если вдруг авторизация идёт из личного кабинета
                    if (!empty($_GET['passport'])) {
                        session(['givenPassportID' => $_GET['passport']]);
                    }

                    session('twi_oauth_tknscr', $twi_oauth_token_secret);

                    return redirect($twi_authorize_url . '?oauth_token=' . $twi_oauth_token);
                }
                break;*/
                case 'vk': {
                    $vk_client_id = '7424907';
                    $vk_redirect_to = 'https://' . $_SERVER['HTTP_HOST'] . '/online/register/';

                    $vk_authorize_url = 'http://oauth.vk.com/authorize';

                    $vk_authorize_params = [
                        'client_id' => $vk_client_id,
                        'scope' => 'status',
                        'redirect_uri' => $vk_redirect_to,
                        'response_type' => 'code'
                    ];

                    session_start();
                    $_SESSION['social'] = 'vk';
                    session(['redirect_uri' => $_GET['redirect_uri']]);

                    return redirect($vk_authorize_url . '?' . urldecode(http_build_query($vk_authorize_params)));
                }
                break;
/*                case 'ok_ru': {
                    # охренительное зрелище: авторизация через OK.ru
                    $ok_client_id = '1249373696';
                    $ok_redirect_to = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . '/register/';
                    $ok_authorize_url = 'https://connect.ok.ru/oauth/authorize';

                    $ok_auth_params = [
                        'client_id' => $ok_client_id,
                        'scope' => 'email',
                        'response_type' => 'code',
                        'redirect_uri' => $ok_redirect_to
                    ];

                    session_start();
                    $_SESSION['social'] = 'OK.ru';
                    session(['redirect_uri' => $_GET['redirect_uri']]);

                    # добавляем ещё кое-что в сессию на тот случай, если вдруг авторизация идёт из личного кабинета
                    if (!empty($_GET['passport'])) {
                        session(['givenPassportID' => $_GET['passport']]);
                    }

                    return redirect($ok_authorize_url . '?' . urldecode(http_build_query($ok_auth_params)));
                }
                break;*/
                default:
                    return 'Unknown social network';
            }
        }

        return NULL;
    }
}