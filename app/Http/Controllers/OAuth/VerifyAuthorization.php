<?php
/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 08.01.2017
 * Time: 16:48
 */

namespace App\Http\Controllers\OAuth;

use App\Http\Controllers\Controller;
use App\OnlinePlayer;
use App\OnlinePlayerSession;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class VerifyAuthorization extends Controller
{
    public function step1()
    {
        session_start();

        if (!isset($_SESSION['social'])) {
            return 'No social network found';
        }

        if ($socialNetwork = $_SESSION['social']) {
            switch ($socialNetwork) {
                /*case 'fb': {
                    $fb_client_id = '1281258195270851';
                    $fb_secret_key = '05c91e0c7afbb06dbb43c59dbf879abc';
                    $fb_redirect_to = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . '/register/';


                    if (isset($_GET['code'])) {
                        # если ответ пришёл, выполняем действия: получает токен доступа к информации, и данные аккаунта
                        $fb_token_request_params = [
                            'client_id' => $fb_client_id,
                            'redirect_uri' => $fb_redirect_to,
                            'client_secret' => $fb_secret_key,
                            'code' => $_GET['code']
                        ];

                        $fb_token_request_url = 'https://graph.facebook.com/v2.8/oauth/access_token';

                        $token_info = json_decode(file_get_contents($fb_token_request_url . '?' . urldecode(http_build_query($fb_token_request_params))));

                        if (!empty($token_info->access_token)) {
                            $fb_request_data_params = [
                                'access_token' => $token_info->access_token,
                                'fields' => 'id,first_name,last_name,gender,education,birthday,about,link,picture.width(160).height(160),email'
                            ];

                            $fb_request_data_url = 'https://graph.facebook.com/me?' . http_build_query($fb_request_data_params);

                            $userInfo = json_decode(file_get_contents($fb_request_data_url));

                            session(['socialNet' => 'facebook']);

                            if ($userInfo->id) {
                                # если пользователь получен, проверяем его email и авторизуемую социальную сеть
                                # 1. если совпадает e-mail, id аккаунта в соцсети и сама соцсеть, то тупо авторизуем
                                # 2. если совпадает e-mail, но данные соцсети не совпали, требуем верификации с выводом
                                #    найденного по e-mail зареганного ранее пользователя
                                # 3. если ничего не совпало, создаём аккаунт
                                # 4. если вдруг нам пришёл конкретный Паспорт ID, получаем $passportUser по ID

                                if (!empty(session('givenPassportID'))) {
                                    $passportUser = DB::table('passport_accounts')->where(['passport_ID' => session('givenPassportID')])->first();
                                } else {
                                    $passportUser = DB::table('passport_accounts')->where(['email' => $userInfo->email])->first();
                                }
                                $socialUser = DB::table('accounts_social')->where([
                                    'social_network' => session('socialNet'),
                                    'social_id' => $userInfo->id
                                ])->first();

                                if (!empty($passportUser->passport_id) || !empty($socialUser->passport_id)) {
                                    # проверяем, если ли вообще в системе пользователь с такой социалкой или email
                                    # это сделано для тех случаев, когда пользователь решит (а мы его попросим хорошо)
                                    # присоединить к его аккаунту несколько социалок
                                    $mainPassportID = (!empty($passportUser->passport_id))
                                        ? $passportUser->passport_id
                                        : $socialUser->passport_id;

                                    if ($mainPassportID) {
                                        # мы нашли пользователя по соцсети, просто авторизуем его в системе
                                        if (!empty($socialUser->passport_id)) {
                                            // авторизуем пользователя, поскольку найден привязанный к passportID
                                            // аккаунт в социальной сети
                                            return Redirect::route('finalize_auth')->with(['passport_id' => $mainPassportID]);
                                        } else if (empty($socialUser->passport_id)) {
                                            // пользователь найден, однако социальная сеть новая. просим подтвердить
                                            // добавление новой учётной записи
                                            // передаём userInfo и passportID, найденный по e-mail
                                            return Redirect::route('new_social_link')->with([
                                                'passport' => $passportUser,
                                                'userData' => $userInfo
                                            ]);
                                        }
                                    }
                                }

                                return Redirect::route('register_step2')->with(['userData' => $userInfo]);
                            }
                        }
                    }
                }
                break;
                case 'google': {
                    $gp_client_id = '325658553609-f6a9hq9ejbd1d3cvu7pt8m3o768bv3da.apps.googleusercontent.com';
                    $gp_secret_key = 'huKzldiN2yX2Ey1-L0shDVfb';
                    $gp_redirect_to = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . '/register/';
                    $gp_token_url = 'https://accounts.google.com/o/oauth2/token';
                    $gp_userInfo_url = 'https://www.googleapis.com/oauth2/v1/userinfo';

                    if (isset($_GET['code'])) {
                        $gp_request_params = [
                            'client_id' => $gp_client_id,
                            'client_secret' => $gp_secret_key,
                            'redirect_uri' => $gp_redirect_to,
                            'grant_type' => 'authorization_code',
                            'code' => $_GET['code']
                        ];

                        $curl = curl_init();
                        curl_setopt($curl, CURLOPT_URL, $gp_token_url);
                        curl_setopt($curl, CURLOPT_POST, 1);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($gp_request_params)));
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                        $result = curl_exec($curl);

                        curl_close($curl);

                        $tokenInfo = json_decode($result);

                        if (!empty($tokenInfo->access_token)) {
                            $gp_request_params['access_token'] = $tokenInfo->access_token;

                            $userInfo = json_decode(file_get_contents($gp_userInfo_url . '?' . urldecode(http_build_query($gp_request_params))));

                            session(['socialNet' => 'google+']);

                            if ($userInfo->id) {
                                # если пользователь получен, проверяем его email и авторизуемую социальную сеть
                                # 1. если совпадает e-mail, id аккаунта в соцсети и сама соцсеть, то тупо авторизуем
                                # 2. если совпадает e-mail, но данные соцсети не совпали, требуем верификации с выводом
                                #    найденного по e-mail зареганного ранее пользователя
                                # 3. если ничего не совпало, создаём аккаунт
                                # 4. если вдруг нам пришёл конкретный Паспорт ID, получаем $passportUser по ID

                                if (!empty(session('givenPassportID'))) {
                                    $passportUser = DB::table('passport_accounts')->where(['passport_ID' => session('givenPassportID')])->first();
                                } else {
                                    $passportUser = DB::table('passport_accounts')->where(['email' => $userInfo->email])->first();
                                }
                                $socialUser = DB::table('accounts_social')->where([
                                    'social_network' => session('socialNet'),
                                    'social_id' => $userInfo->id
                                ])->first();

                                if (!empty($passportUser->passport_id) || !empty($socialUser->passport_id)) {
                                    # проверяем, если ли вообще в системе пользователь с такой социалкой или email
                                    # это сделано для тех случаев, когда пользователь решит (а мы его попросим хорошо)
                                    # присоединить к его аккаунту несколько социалок
                                    $mainPassportID = (!empty($passportUser->passport_id))
                                        ? $passportUser->passport_id
                                        : $socialUser->passport_id;

                                    if ($mainPassportID) {
                                        # мы нашли пользователя по соцсети, просто авторизуем его в системе
                                        if (!empty($socialUser->passport_id)) {
                                            // авторизуем пользователя, поскольку найден привязанный к passportID
                                            // аккаунт в социальной сети
                                            return Redirect::route('finalize_auth')->with(['passport_id' => $mainPassportID]);
                                        } else if (empty($socialUser->passport_id)) {
                                            // пользователь найден, однако социальная сеть новая. просим подтвердить
                                            // добавление новой учётной записи
                                            // передаём userInfo и passportID, найденный по e-mail
                                            return Redirect::route('new_social_link')->with([
                                                'passport' => $passportUser,
                                                'userData' => $userInfo
                                            ]);
                                        }
                                    }
                                }

                                return Redirect::route('register_step2')->with(['userData' => $userInfo]);
                            }
                        }
                    }
                }
                break;
                case 'twitter': {
                    // авторизация через твиттер (самая задротская часть, которую только можно было представить)
                    $twi_consumer_key = 'PVoFAWgo44FhFt8ClUVDptyKq';
                    $twi_consumer_secret = 'bXu3B9vBdtFyb65Je8FG4gWwFpmFX1x2TMDoXwPGRsaEblZM3i';

                    $twi_access_token_url = 'https://api.twitter.com/oauth/access_token';
                    $twi_account_additional_url = 'https://api.twitter.com/1.1/account/verify_credentials.json';

                    if (!empty($_GET['oauth_token']) && !empty($_GET['oauth_verifier'])) {
                        $oauth_nonce = md5(uniqid(rand(), true));
                        $oauth_timestamp = time();

                        $twi_oauth_token = $_GET['oauth_token'];
                        $twi_oauth_verifier = $_GET['oauth_verifier'];

                        $oauth_base_text = 'GET&' . urlencode($twi_access_token_url) . '&';

                        $twi_signature_params = [
                            'oauth_consumer_key=' . $twi_consumer_key . '&',
                            'oauth_nonce=' . $oauth_nonce . '&',
                            'oauth_signature_method=HMAC-SHA1' . '&',
                            'oauth_token=' . $twi_oauth_token . '&',
                            'oauth_timestamp=' . $oauth_timestamp . '&',
                            'oauth_version=1.0'
                        ];

                        $tknscr = session('twi_oauth_tknscr');

                        $key = $twi_consumer_secret . '&' . $tknscr;
                        $oauth_base_text = 'GET' . '&' . urlencode($twi_access_token_url) . '&' . implode('', array_map('urlencode', $twi_signature_params));
                        $twi_oauth_signature = base64_encode(hash_hmac('sha1', $oauth_base_text, $key, true));

                        $twi_2nd_request_params = [
                            'oauth_nonce=' . $oauth_nonce,
                            'oauth_signature_method=HMAC-SHA1',
                            'oauth_timestamp=' . $oauth_timestamp,
                            'oauth_consumer_key=' . $twi_consumer_key,
                            'oauth_token=' . $twi_oauth_token,
                            'oauth_verifier=' . $twi_oauth_verifier,
                            'oauth_signature=' . $twi_oauth_signature,
                            'oauth_version=1.0'
                        ];

                        $twi_url_req = $twi_access_token_url . '?' . implode('&', $twi_2nd_request_params);
                        $twi_2nd_request_response = file_get_contents($twi_url_req);
                        parse_str($twi_2nd_request_response, $twi_2nd_request_result);

                        if (!empty($twi_2nd_request_result['oauth_token'])) {
                            # первоначальные данные получены, теперь получаем полноценные данные о пользователе
                            $oauth_nonce = md5(uniqid(rand(), true));
                            $oauth_timestamp = time();

                            $twi_oauth_token = $twi_2nd_request_result['oauth_token'];
                            $twi_oauth_token_secret = $twi_2nd_request_result['oauth_token_secret'];
                            $screen_name = $twi_2nd_request_result['screen_name'];

                            $twi_profile_signature_params = [
                                'include_email=true' . '&',
                                'oauth_consumer_key=' . $twi_consumer_key . '&',
                                'oauth_nonce=' . $oauth_nonce . '&',
                                'oauth_signature_method=HMAC-SHA1' . '&',
                                'oauth_timestamp=' . $oauth_timestamp . '&',
                                'oauth_token=' . $twi_oauth_token . '&',
                                'oauth_version=1.0' . '&',
                                'screen_name=' . $screen_name
                            ];

                            $oauth_base_text = 'GET' . '&' . urlencode($twi_account_additional_url) . '&' . implode('', array_map('urlencode', $twi_profile_signature_params));

                            $key = $twi_consumer_secret . '&' . $twi_oauth_token_secret;
                            $twi_profile_signature = base64_encode(hash_hmac('sha1', $oauth_base_text, $key, true));

                            $twi_profile_request_params = [
                                'include_email=true',
                                'oauth_consumer_key=' . $twi_consumer_key,
                                'oauth_nonce=' . $oauth_nonce,
                                'oauth_signature=' . urlencode($twi_profile_signature),
                                'oauth_signature_method=HMAC-SHA1',
                                'oauth_timestamp=' . $oauth_timestamp,
                                'oauth_token=' . urlencode($twi_oauth_token),
                                'oauth_version=1.0',
                                'screen_name=' . $screen_name
                            ];

                            $twi_url_profile_req = $twi_account_additional_url . '?' . implode('&', $twi_profile_request_params);
                            $twi_profile_response = file_get_contents($twi_url_profile_req);

                            $twi_profile = json_decode($twi_profile_response);

                            session(['socialNet' => 'twitter']);

                            if ($twi_profile->id) {
                                # если пользователь получен, проверяем его email и авторизуемую социальную сеть
                                # 1. если совпадает e-mail, id аккаунта в соцсети и сама соцсеть, то тупо авторизуем
                                # 2. если совпадает e-mail, но данные соцсети не совпали, требуем верификации с выводом
                                #    найденного по e-mail зареганного ранее пользователя
                                # 3. если ничего не совпало, создаём аккаунт
                                # 4. если вдруг нам пришёл конкретный Паспорт ID, получаем $passportUser по ID

                                if (!empty(session('givenPassportID'))) {
                                    $passportUser = DB::table('passport_accounts')->where(['passport_ID' => session('givenPassportID')])->first();
                                } else {
                                    $passportUser = DB::table('passport_accounts')->where(['email' => $twi_profile->email])->first();
                                }
                                $socialUser = DB::table('accounts_social')->where([
                                    'social_network' => session('socialNet'),
                                    'social_id' => $twi_profile->id
                                ])->first();

                                if (!empty($passportUser->passport_id) || !empty($socialUser->passport_id)) {
                                    # проверяем, если ли вообще в системе пользователь с такой социалкой или email
                                    # это сделано для тех случаев, когда пользователь решит (а мы его попросим хорошо)
                                    # присоединить к его аккаунту несколько социалок
                                    $mainPassportID = (!empty($passportUser->passport_id))
                                        ? $passportUser->passport_id
                                        : $socialUser->passport_id;

                                    if ($mainPassportID) {
                                        # мы нашли пользователя по соцсети, просто авторизуем его в системе
                                        if (!empty($socialUser->passport_id)) {
                                            // авторизуем пользователя, поскольку найден привязанный к passportID
                                            // аккаунт в социальной сети
                                            return Redirect::route('finalize_auth')->with(['passport_id' => $mainPassportID]);
                                        } else if (empty($socialUser->passport_id)) {
                                            // пользователь найден, однако социальная сеть новая. просим подтвердить
                                            // добавление новой учётной записи
                                            // передаём userInfo и passportID, найденный по e-mail
                                            return Redirect::route('new_social_link')->with([
                                                'passport' => $passportUser,
                                                'userData' => $twi_profile
                                            ]);
                                        }
                                    }
                                }

                                return Redirect::route('register_step2')->with(['userData' => $twi_profile]);
                            }
                        }
                    }
                }
                break;*/
                case 'vk': {
                    $vk_client_id = '7424907';
                    $vk_client_secret = 'cbvJdoOmnWR6JQoDJSwF';
                    $vk_redirect_to = 'https://' . $_SERVER['HTTP_HOST'] . '/online/register/';
                    $vk_access_token_url = 'https://oauth.vk.com/access_token';
                    $vk_get_user_url = 'https://api.vk.com/method/users.get';

                    if (isset($_GET['code'])) {
                        $vk_request_token_params = [
                            'v' => '5.81',
                            'client_id' => $vk_client_id,
                            'client_secret' => $vk_client_secret,
                            'code' => $_GET['code'],
                            'redirect_uri' => $vk_redirect_to
                        ];

                        $vk_token = json_decode(file_get_contents($vk_access_token_url . '?' . urldecode(http_build_query($vk_request_token_params))));

                        if (!empty($vk_token->access_token)) {
                            $vk_request_user_params = [
                                'v' => '5.81',
                                'uids' => $vk_token->user_id,
                                'fields' => 'uid,first_name,last_name,screen_name,sex,bdate,photo_200,about,activities,career,city,common_count,connections,country,education,followers_count,home_town,schools,timezone,universities',
                                'access_token' => $vk_token->access_token
                            ];

                            $vk_userInfo = json_decode(file_get_contents($vk_get_user_url . '?' . urldecode(http_build_query($vk_request_user_params))), true);

                            $vk_user = $vk_userInfo['response'][0];

//                            $vk_user['email'] = $vk_token->email;

                            $vk_user = json_decode(json_encode($vk_user));

                            session(['socialNet' => 'vk']);

                            if (!empty($vk_user->id)) {
                                # если пользователь получен, то мы сначала пытаемся его добавить в систему, а затем авторизуем
                                $searchOnlinePlayer = OnlinePlayer::whereVkId($vk_user->id)->first();
                                $sesskey = '';

                                if ($searchOnlinePlayer) {
                                    # проверяем время жизни сессии. если существует в рамках времени, то обновляем. если нет, то задаём новое
                                    $existingSession = OnlinePlayerSession::wherePlayerId($searchOnlinePlayer->id)
                                        ->where('valid_through', '>', Carbon::now('Europe/Moscow')->toDateTimeString())->first();
                                    if ($existingSession) {
                                        # обновляем время сессии: текущее время + 10 минут
                                        $existingSession->valid_through = Carbon::now('Europe/Moscow')->addMinutes(10)->toDateTimeString();
                                        $existingSession->save();
                                        $sesskey = $existingSession->session_key;
                                    } else {
                                        # и создаём сессию для пользователя продолжительностью в 10 минут
                                        $newOnlinePlayerSession = new OnlinePlayerSession();
                                        $newOnlinePlayerSession->player_id = $searchOnlinePlayer->id;
                                        $newOnlinePlayerSession->session_key = md5($searchOnlinePlayer->id . '__rr_' . uniqid());
                                        $newOnlinePlayerSession->valid_through = Carbon::now('Europe/Moscow')->addMinutes(10)->toDateTimeString();
                                        $newOnlinePlayerSession->save();
                                        $sesskey = $newOnlinePlayerSession->session_key;
                                    }
                                } else {
                                    # добавляем нового игрока, заполняя все необходимые данные
                                    $newOnlinePlayer = new OnlinePlayer();
                                    $newOnlinePlayer->first_name = $vk_user->first_name;
                                    $newOnlinePlayer->last_name = $vk_user->last_name;
                                    $newOnlinePlayer->vk_id = $vk_user->id;
                                    $newOnlinePlayer->vk_avatar = $vk_user->photo_200;
                                    $newOnlinePlayer->vk_sex = $vk_user->sex;
                                    if (!empty($vk_user->screen_name)) {
                                        $newOnlinePlayer->vk_screen_name = $vk_user->screen_name;
                                    }
                                    $newOnlinePlayer->save();

                                    # и сразу создаём сессию для пользователя продолжительностью в 10 минут
                                    $newOnlinePlayerSession = new OnlinePlayerSession();
                                    $newOnlinePlayerSession->player_id = $newOnlinePlayer->id;
                                    $newOnlinePlayerSession->session_key = md5($newOnlinePlayer->id . '__rr_' . uniqid());
                                    $newOnlinePlayerSession->valid_through = Carbon::now('Europe/Moscow')->addMinutes(10)->toDateTimeString();
                                    $newOnlinePlayerSession->save();
                                    $sesskey = $newOnlinePlayerSession->session_key;
                                }

                                $cookie = cookie('online_sessid', $sesskey);
                                return Redirect::route('online.app')->with(['userData' => $vk_user])->cookie($cookie);
                            }
                        }
                    }
                }
                break;
                /*case 'OK.ru': {
                    # продолжение крутой авторизации через "Одноклассников"
                    $ok_client_id = '1249373696';
                    $ok_public_key = 'CBAPOIHLEBABABABA';
                    $ok_secret_key = '45EF5DDD9AE48F221CD0227F';

                    $ok_redirect_to = (isset($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . '/register/';
                    $ok_request_token_url = 'https://api.ok.ru/oauth/token.do';

                    if (isset($_GET['code'])) {
                        $ok_request_token_params = [
                            'code' => $_GET['code'],
                            'redirect_uri' => $ok_redirect_to,
                            'grant_type' => 'authorization_code',
                            'client_id' => $ok_client_id,
                            'client_secret' => $ok_secret_key
                        ];

                        $curl = curl_init();
                        curl_setopt($curl, CURLOPT_URL, $ok_request_token_url);
                        curl_setopt($curl, CURLOPT_POST, 1);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($ok_request_token_params)));
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

                        $result = curl_exec($curl);
                        curl_close($curl);

                        $ok_token_info = json_decode($result);

                        session(['socialNet' => 'OK.ru']);

                        if (!empty($ok_token_info->access_token) && !empty($ok_public_key)) {
                            $ok_request_sign = md5("application_key={$ok_public_key}format=jsonmethod=users.getCurrentUser" .
                                md5("{$ok_token_info->access_token}{$ok_secret_key}"));

                            $ok_user_request_params = [
                                'method' => 'users.getCurrentUser',
                                'access_token' => $ok_token_info->access_token,
                                'application_key' => $ok_public_key,
                                'format' => 'json',
                                'sig' => $ok_request_sign
                            ];

                            $ok_userInfo = json_decode(file_get_contents('https://api.ok.ru/fb.do?' . urldecode(http_build_query($ok_user_request_params))));

                            if ($ok_userInfo->uid) {
                                # если пользователь получен, проверяем его email и авторизуемую социальную сеть
                                # 1. если совпадает e-mail, id аккаунта в соцсети и сама соцсеть, то тупо авторизуем
                                # 2. если совпадает e-mail, но данные соцсети не совпали, требуем верификации с выводом
                                #    найденного по e-mail зареганного ранее пользователя
                                # 3. если ничего не совпало, создаём аккаунт
                                # 4. если вдруг нам пришёл конкретный Паспорт ID, получаем $passportUser по ID

                                if (!empty(session('givenPassportID'))) {
                                    $passportUser = DB::table('passport_accounts')->where(['passport_ID' => session('givenPassportID')])->first();
                                } else {
                                    $passportUser = DB::table('passport_accounts')->where(['email' => $ok_userInfo->email])->first();
                                }
                                $socialUser = DB::table('accounts_social')->where([
                                    'social_network' => session('socialNet'),
                                    'social_id' => $ok_userInfo->uid
                                ])->first();

                                if (!empty($passportUser->passport_id) || !empty($socialUser->passport_id)) {
                                    # проверяем, если ли вообще в системе пользователь с такой социалкой или email
                                    # это сделано для тех случаев, когда пользователь решит (а мы его попросим хорошо)
                                    # присоединить к его аккаунту несколько социалок
                                    $mainPassportID = (!empty($passportUser->passport_id))
                                        ? $passportUser->passport_id
                                        : $socialUser->passport_id;

                                    if ($mainPassportID) {
                                        # мы нашли пользователя по соцсети, просто авторизуем его в системе
                                        if (!empty($socialUser->passport_id)) {
                                            // авторизуем пользователя, поскольку найден привязанный к passportID
                                            // аккаунт в социальной сети
                                            return Redirect::route('finalize_auth')->with(['passport_id' => $mainPassportID]);
                                        } else if (empty($socialUser->passport_id)) {
                                            // пользователь найден, однако социальная сеть новая. просим подтвердить
                                            // добавление новой учётной записи
                                            // передаём userInfo и passportID, найденный по e-mail
                                            return Redirect::route('new_social_link')->with([
                                                'passport' => $passportUser,
                                                'userData' => $ok_userInfo
                                            ]);
                                        }
                                    }
                                }

                                return Redirect::route('register_step2')->with(['userData' => $ok_userInfo]);
                            }
                        }
                    }
                }
                break;*/
                default:
                    return 'Unknown social network';
            }
        }

        return NULL;
    }

    public function step2() {
        if (session('userData')) {
            return view('verify', ['userData' => session('userData')]);
        } else {
            return 'WUT';
        }
    }
}