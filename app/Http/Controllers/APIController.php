<?php

namespace App\Http\Controllers;

use App\EditorRelease;
use App\GameRelease;
use App\NewsArticle;
use App\OnlinePlayer;
use App\OnlinePlayerStat;
use App\OnlinePlayerTransaction;
use App\WebsitePage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class APIController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    private function checkRequestMoneyIsValid(Request $request) {
        $str = '';
        $str .= $request->get('notification_type') . '&';
        $str .= $request->get('operation_id') . '&';
        $str .= $request->get('amount') . '&';
        $str .= $request->get('currency') . '&';
        $str .= $request->get('datetime') . '&';
        $str .= $request->get('sender') . '&';
        $str .= $request->get('codepro') . '&';
        $str .= 'ZLuCV/crwUbKSP9L4UxgfT3Z' . '&';
        $str .= $request->get('label');

        $secretHash = sha1($str);

        return ($secretHash == $request->get('sha1_hash'));
    }

    public function receiveMoney(Request $request) {
        Log::info('MONEY RECEIVE', ['r' => $request]);

        ## Отвалидировать запрос
        if ($this->checkRequestMoneyIsValid($request)) {
            # Если валидация пройдена, то зачисляем транзакцию
            $newOnlinePlayerTransaction = new OnlinePlayerTransaction();
            $newOnlinePlayerTransaction->player_id = 0;
            $newOnlinePlayerTransaction->operation_id = $request->get('operation_id');
            $newOnlinePlayerTransaction->type = $request->get('notification_type');
            $newOnlinePlayerTransaction->amount = $request->get('amount');
            if (!empty($request->get('withdraw_amount'))) {
                $newOnlinePlayerTransaction->amount = $request->get('withdraw_amount');
                $newOnlinePlayerTransaction->received = $request->get('amount');
            } else {
                $newOnlinePlayerTransaction->received = $newOnlinePlayerTransaction->amount;
            }
            # вычисляем, сколько попыток зачислять
            # 1. если сумма 150 ₽ или около того - 100 попыток
            # 2. если сумма 300 ₽ или около того - 500 попыток
            # 3. если сумма 500 ₽ или около того - 1000 попыток
            if ($newOnlinePlayerTransaction->amount >= 100 && $newOnlinePlayerTransaction->amount < 200) {
                $newOnlinePlayerTransaction->takes_added = 100;
            } else if ($newOnlinePlayerTransaction->amount >= 250 && $newOnlinePlayerTransaction->amount < 350) {
                $newOnlinePlayerTransaction->takes_added = 500;
            } else if ($newOnlinePlayerTransaction->amount >= 450) {
                $newOnlinePlayerTransaction->takes_added = 1000;
            } else {
                $newOnlinePlayerTransaction->takes_added = 0;
            }

            # вычисляем, сколько уходит в призовой фонд (25% от суммы зачисления)
            $toRaiseFund = floatval($newOnlinePlayerTransaction->amount) * 0.25;
            $newOnlinePlayerTransaction->raised_fund_amount = $toRaiseFund;

            # записываем все метки
            if (!empty($request->get('label'))) {
                $newOnlinePlayerTransaction->label = $request->get('label');
                # в label пишется player_id, как это классно я прописал
                $newOnlinePlayerTransaction->player_id = intval($request->get('label'));
            }
            if (!empty($request->get('operation_label'))) {
                $newOnlinePlayerTransaction->operation_label = $request->get('operation_label');
            }

            # записываем метку времени платежа
            $timeToPay = Carbon::parse($request->get('datetime'), 'Europe/Moscow');
            $newOnlinePlayerTransaction->payed_timestamp = $timeToPay->toDateTimeString();
            $newOnlinePlayerTransaction->save();

            # финальное - находим игрока в базе, и зачисляем ему попытки
            $onlinePlayer = OnlinePlayer::find($newOnlinePlayerTransaction->player_id);
            if ($onlinePlayer) {
                $onlinePlayer->game_takes = $onlinePlayer->game_takes + $newOnlinePlayerTransaction->takes_added;
                $onlinePlayer->timestamps = false;
                $onlinePlayer->save();
            }

            return response()->json(['status' => 'money received'], 200);
        } else {
            return response()->json(['status' => 'money not received'], 200);
        }
    }
}
