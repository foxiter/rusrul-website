<?php

namespace App\Http\Controllers;

use App\EditorRelease;
use App\GameRelease;
use App\NewsArticle;
use App\OnlinePlayer;
use App\OnlinePlayerSession;
use App\OnlinePlayerStat;
use App\OnlinePlayerTransaction;
use App\WebsitePage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;

class OnlineGameController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('online_game');
    }

    public function index(Request $request) {
        $page_title = 'Онлайн-игра: сражение против механизма';
        $session_game = Cookie::get('online_sessid');

        return view('front.online.app', compact('page_title', 'session_game'));
    }

    # получение игрока
    public function getPlayer(Request $request) {
        # пытаемся найти по сессии игрока
        $currentSession = OnlinePlayerSession::whereSessionKey($request->get('sessid'))->first();

        if ($currentSession) {
            # получаем игрока
            $currentSessionPlayer = OnlinePlayer::find($currentSession->player_id);
            if ($currentSessionPlayer) {
                return response()->json([
                    'status' => 'success',
                    'data' => [
                        'name' => $currentSessionPlayer->first_name . ' ' . $currentSessionPlayer->last_name,
                        'avatar' => $currentSessionPlayer->vk_avatar,
                        'player_id' => $currentSessionPlayer->id,
                        'session_data' => $currentSession->session_key,
                        'takes' => $currentSessionPlayer->game_takes
                    ]
                ]);
            } else {
                return response()->json(['status' => 'error', 'errno' => '1001', 'message' => 'Player not found'], 401);
            }
        } else {
            return response()->json(['status' => 'error', 'errno' => '1010', 'message' => 'Session not found, or expired'], 401);
        }
    }

    # получение списка TOP TEN
    public function getTopPlayers(Request $request) {
        $topTenPlayers = OnlinePlayer::query()->orderBy('total_points', 'desc')->limit(5)->get();

        $topTenPlayersArray = [];
        foreach ($topTenPlayers as $topTenPlayer) {
            $__top10Pl = [];

            $__top10Pl['name'] = $topTenPlayer->first_name;
            $__top10Pl['avatar'] = $topTenPlayer->vk_avatar;
            //$__top10Pl['points'] = $topTenPlayer->total_points;

            $topTenPlayersArray[] = $__top10Pl;
        }

        $playerPosition = null;
        $playerName = null;
        $playerTotalPoints = null;
        $playerTakes = null;

        # считаем, сколько у нас составляет призовой фонд
        $additionalRaise = OnlinePlayerTransaction::query()->sum('raised_fund_amount');
        $prizeFund = 2000 + $additionalRaise;

        # пытаемся найти по сессии игрока
        $currentSession = OnlinePlayerSession::whereSessionKey($request->get('sessid'))->first();
        if ($currentSession) {
            # находим позицию игрока в турнирной таблице
            $allPlayers = OnlinePlayer::query()->orderBy('total_points', 'desc')->get();
            $pos = 1;
            foreach ($allPlayers as $player) {
                if ($player->id == $currentSession->player_id) {
                    $playerPosition = $pos;
                    $playerName = $player->first_name;
                    $playerTotalPoints = $player->total_points;
                    $playerTakes = $player->game_takes;
                }
                $pos++;
            }
        }

        return response()->json(['status' => 'success', 'prize_fund' => $prizeFund, 'top10' => $topTenPlayersArray, 'player_stats' => ['position' => $playerPosition, 'name' => $playerName, 'points' => $playerTotalPoints, 'takes' => $playerTakes]]);
    }

    # совершение действия. работает на каждое нажатие кнопки. если происходит отвал по авторизации, то редиректим на страницу входа
    # если же авторизация ещё работает, то в случае, если останется меньше 5 минут, срок действия токена продлевается ещё на 5 минут
    public function makeAction(Request $request) {
        if (!empty($request->get('sessid')) && !empty($request->get('player'))) {
            $currentSession = OnlinePlayerSession::whereSessionKey($request->get('sessid'))
                ->wherePlayerId($request->get('player'))->first();
            if ($currentSession) {
                $currentTime = Carbon::now('Europe/Moscow');
                $sessionTime = Carbon::createFromFormat('Y-m-d H:i:s', $currentSession->valid_through);

                if ($sessionTime->diffInMinutes($currentTime) < 5) {
                    $currentSession->valid_through = $sessionTime->addMinutes(5);
                    $currentSession->save();

                    return response()->json(['status' => 'success', 'message' => 'Pong.']);
                } else {
                    return response()->json(['status' => 'success', 'message' => 'Pong.']);
                }
            } else {
                return response()->json(['status' => 'error', 'errno' => '1010', 'message' => 'Session not found, or expired'], 401);
            }
        } else {
            return response()->json(['status' => 'error', 'errno' => '1010', 'message' => 'Session not found, or expired'], 401);
        }
    }

    # сохранение результатов
    # ВАЖНО! Результаты необходимо перепроверять, во избежание подделки результатов.
    public function saveResults(Request $request) {
        # пробуем найти текущую сессию по ID игрока и ID сессии. если чего-то из этого не прилетает, то выбиваем ошибку
        if (!empty($request->get('player')) && !empty($request->get('sessid'))) {
            $currentSession = OnlinePlayerSession::whereSessionKey($request->get('sessid'))->wherePlayerId($request->get('player'))->first();

            if ($currentSession) {
                # получаем игрока
                $currentSessionPlayer = OnlinePlayer::find($currentSession->player_id);
                if ($currentSessionPlayer) {
                    $currentMoney = (!empty($request->get('money'))) ? $request->get('money') : 0;
                    $currentResult = $request->get('result');
                    $currentWinMoney = (!empty($request->get('win_money'))) ? $request->get('win_money') : 0;
                    $currentLoseMoney = (!empty($request->get('lose_money'))) ? $request->get('lose_money') : 0;
                    $currentPlayerId = $request->get('player');
                    $currentControlHash = $request->get('checkword');

                    # теперь формируем контрольную строку, и сверяем её с прилетелвшей контрольной строкой (античит)
                    $checkWord = md5($currentMoney . '__' . $currentResult . '__'.(($currentResult=='win')?'+':'-').'_' . $currentWinMoney . '_' . $currentLoseMoney . '~' . $currentPlayerId);
                    $checkWord2 = md5($currentPlayerId . '_' . $currentLoseMoney . '_' . $currentWinMoney . '_' . $currentResult . '_' . $currentMoney);
                    $checkWord = md5($checkWord . '___' . $checkWord2);
                    if ($checkWord == $currentControlHash && in_array(intval($currentResult), [0, 1, 2500, 5000, 15000, 30000, 40000, 80000, 90000, 180000, 1180000])) {
                        # сохраняем все результаты
                        ## + запись в стату
                        $newStatInfo = new OnlinePlayerStat();
                        $newStatInfo->player_id = $currentSessionPlayer->id;
                        if ($currentResult == 'win') {
                            $newStatInfo->win = 1;
                        } else {
                            $newStatInfo->lose = 1;
                        }
                        $newStatInfo->total_win = $currentWinMoney;
                        $newStatInfo->total_lose = $currentLoseMoney;
                        $newStatInfo->save();

                        # наращиваем статистику выигрышей
                        $currentSessionPlayer->games_played = $currentSessionPlayer->games_played + 1;
                        if ($currentResult == 'win') {
                            $currentSessionPlayer->games_win = $currentSessionPlayer->games_win + 1;
                        } else {
                            $currentSessionPlayer->games_lose = $currentSessionPlayer->games_lose + 1;
                        }
                        $currentSessionPlayer->total_points = $currentSessionPlayer->total_points + $currentMoney;

                        $takes = $currentSessionPlayer->game_takes;
                        if (intval($currentWinMoney) == 15000) {  # провал на 3-м раунде
                            $takes = $takes - 1;
                        }
                        if (intval($currentWinMoney) == 40000) {  # провал на 4-м раунде
                            $takes = $takes - 2;
                        }
                        if (intval($currentWinMoney) == 90000) {  # провал на 5-м раунде
                            $takes = $takes - 5;
                        }
                        $takes--;
                        $currentSessionPlayer->game_takes = $takes;

                        $currentSessionPlayer->save();

                        return response()->json([
                            'status' => 'success',
                            'message' => 'Result saved successfully.'
                        ]);
                    } else {
                        # выдаём ошибку сохранения, поскольку сработал античит
                        return response()->json([
                            'status' => 'error',
                            'errno' => '1234',
                            'message' => 'Incorrect game data for save, aborted!'
                        ], 422);
                    }
                } else {
                    return response()->json(['status' => 'error', 'errno' => '1001', 'message' => 'Player not found'], 401);
                }
            } else {
                return response()->json(['status' => 'error', 'errno' => '1010', 'message' => 'Session not found, or expired'], 401);
            }
        } else {
            return response()->json(['status' => 'error', 'errno' => '1010', 'message' => 'Session not found, or expired'], 401);
        }
    }
}
