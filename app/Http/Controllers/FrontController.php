<?php

namespace App\Http\Controllers;

use App\EditorRelease;
use App\GameRelease;
use App\NewsArticle;
use App\OnlinePlayer;
use App\OnlinePlayerStat;
use App\WebsitePage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class FrontController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newsOnHomePage = NewsArticle::with('author')
            ->whereShowOnHomepage(1)
            ->where('publish_date', '<=', date('Y-m-d H:i:s'))
            ->orderBy('publish_date', 'desc')->first();
        $newsDate = $this->convertDateToHuman($newsOnHomePage->publish_date);

        $lastGameRelease = GameRelease::orderBy('release_date', 'desc')->first();
        $lastEditorRelease = EditorRelease::orderBy('release_date', 'desc')->first();

        return view('front.main', compact('newsOnHomePage', 'newsDate', 'lastGameRelease', 'lastEditorRelease'));
    }

    private function convertDateToHuman($date) {
        $dateItems = explode(' ', $date);
        $dateFull = $dateItems[0];

        $dateFullSegments = explode('-', $dateFull);

        $months = [
            1 => 'января',
            2 => 'февраля',
            3 => 'марта',
            4 => 'апреля',
            5 => 'мая',
            6 => 'июня',
            7 => 'июля',
            8 => 'августа',
            9 => 'сентября',
            10 => 'октября',
            11 => 'ноября',
            12 => 'декабря',
        ];

        return $dateFullSegments[2] . ' ' . $months[intval($dateFullSegments[1])] . ' ' . $dateFullSegments[0];
    }

    public function showNewsList(Request $request) {
        $page_title = 'Новости проекта';

        $newsList = NewsArticle::whereIsPublished(1)->where('publish_date', '<=', date('Y-m-d H:i:s'))
            ->orderBy('publish_date', 'desc')->paginate(5);

        return view('front.news.main', compact('newsList', 'page_title'));
    }

    public function showNewsItem($code, Request $request) {
        $chosenNewsItem = NewsArticle::whereUrl($code)->whereIsPublished(1)->first();

        if (!$chosenNewsItem)
            App::abort(404);

        $page_title = $chosenNewsItem->title;

        return view('front.news.item', compact('chosenNewsItem', 'page_title'));
    }

    public function getPage($slug) {
        $chosenPage = WebsitePage::whereUrlPart($slug)->first();

        if (!$chosenPage)
            App::abort(404);

        $page_title = $chosenPage->title;

        return view('front.single_page', compact('chosenPage', 'page_title'));
    }

    public function viewPcGameIndex(Request $request) {
        # временно отправляем всех на страницу с текущим релизом; как только придумаем суть страницы, исправим
        $lastGameRelease = GameRelease::orderBy('release_date', 'desc')->first();

        return redirect()->route('pc.release', ['version' => $lastGameRelease->version]);
    }

    public function viewPcGameRelease($version, Request $request) {
        $chosenGameRelease = GameRelease::whereVersion($version)->first();

        if (!$chosenGameRelease)
            App::abort(404);

        $page_title = 'ПК-игра: скачать версию ' . $version;

        return view('front.pc.release', compact('chosenGameRelease', 'page_title'));
    }

    public function viewQuestionsEditorRelease($version, Request $request) {
        $chosenEditorRelease = EditorRelease::whereVersion($version)->first();

        if (!$chosenEditorRelease)
            App::abort(404);

        $page_title = 'Редактор вопросов для ПК-игры: скачать версию ' . $version;

        return view('front.editor.release', compact('chosenEditorRelease', 'page_title'));
    }

    # Онлайн-игра. Авторизация через VK и переход к версии без сохранения результатов
    public function onlineGameMainPage(Request $request) {
        $page_title = 'Онлайн-игра против механизма';

        $topThreePlayers = OnlinePlayer::query()->orderBy('total_points', 'desc')->limit(3)->get();
        $firstToWinTheGameTopPlayers = OnlinePlayerStat::with('player')->whereTotalWin(1180000)->orderBy('id')->limit(3)->get();

        return view('front.online.index', compact('page_title', 'topThreePlayers', 'firstToWinTheGameTopPlayers'));
    }

    public function receiveMoney(Request $request) {


        Log::info('MONEY RECEIVE', ['r' => $request]);

        return response()->json(['status' => 'money received'], 200);
    }
}
