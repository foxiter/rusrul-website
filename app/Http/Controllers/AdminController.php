<?php

namespace App\Http\Controllers;

use App\EditorRelease;
use App\GameRelease;
use App\NewsArticle;
use App\OnlinePlayer;
use App\OnlinePlayerSession;
use App\OnlinePlayerStat;
use App\OnlinePlayerTransaction;
use App\User;
use App\WebsitePage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lastNewsList = NewsArticle::with('author')->orderBy('publish_date', 'desc')->take(5)->get();
        $activeOnlinePlayers = OnlinePlayerSession::where('valid_through', '>', Carbon::now('Europe/Moscow')->toDateTimeString())->count();

        return view('back.index', compact('lastNewsList', 'activeOnlinePlayers'));
    }

    public function addNewsItem() {
        return view('back.news.add');
    }

    public function saveNewsItem(Request $request) {
        $newNewsItem = new NewsArticle();

        $newNewsItem->title = $request->get('title');
        $newNewsItem->url = $request->get('url');
        $newNewsItem->short_text = $request->get('short_text');
        $newNewsItem->full_text = $request->get('full_text');
        if ($request->get('publish_date')) {
            $newNewsItem->publish_date = date('Y-m-d H:i:s', strtotime($request->input('publish_date')));
        }
        $newNewsItem->user_created = Auth::user()->id;
        $newNewsItem->is_published = ($request->has('is_published')) ? 1 : 0;
        $newNewsItem->show_on_homepage = ($request->has('show_on_homepage')) ? 1 : 0;

        $newNewsItem->save();

        return redirect()->route('admin.dashboard');
    }

    public function addNewPage() {
        return view('back.pages.add');
    }

    public function saveNewPage(Request $request) {
        $newWebsitePage = new WebsitePage();

        $newWebsitePage->title = $request->get('title');
        $newWebsitePage->url_part = $request->get('url_part');
        $newWebsitePage->page_content = $request->get('page_content');

        $newWebsitePage->save();

        return redirect()->route('admin.dashboard');
    }

    public function listGameReleases() {
        $releasesList = GameRelease::orderBy('release_date', 'desc')->get();

        return view('back.game_releases.index', compact('releasesList'));
    }

    public function newGameRelease() {
        return view('back.game_releases.add');
    }

    public function createGameRelease(Request $request) {
        $newGameRelease = new GameRelease();

        $newGameRelease->major_version = $request->get('major_version');
        $newGameRelease->minor_version = $request->get('minor_version');
        $newGameRelease->version = $request->get('version');
        if ($request->get('release_date')) {
            $newGameRelease->release_date = date('Y-m-d H:i:s', strtotime($request->input('release_date')));
        }
        $newGameRelease->description = $request->get('description');
        $newGameRelease->download_link = $request->get('download_link');
        if (file_exists(public_path() . $newGameRelease->download_link)) {
            $newGameRelease->filesize = filesize(public_path() . $newGameRelease->download_link);
        }
        $newGameRelease->is_beta = ($request->has('is_beta')) ? 1 : 0;
        $newGameRelease->save();

        return redirect()->route('admin.pc_game.list');
    }

    public function listEditorReleases() {
        $releasesList = EditorRelease::orderBy('release_date', 'desc')->get();

        return view('back.editor_releases.index', compact('releasesList'));
    }

    public function newEditorRelease() {
        return view('back.editor_releases.add');
    }

    public function createEditorRelease(Request $request) {
        $newEditorRelease = new EditorRelease();

        $newEditorRelease->major_version = $request->get('major_version');
        $newEditorRelease->minor_version = $request->get('minor_version');
        $newEditorRelease->version = $request->get('version');
        if ($request->get('release_date')) {
            $newEditorRelease->release_date = date('Y-m-d H:i:s', strtotime($request->input('release_date')));
        }
        $newEditorRelease->description = $request->get('description');
        $newEditorRelease->download_link = $request->get('download_link');
        if (file_exists(public_path() . $newEditorRelease->download_link)) {
            $newEditorRelease->filesize = filesize(public_path() . $newEditorRelease->download_link);
        }
        $newEditorRelease->save();

        return redirect()->route('admin.qeditor.list');
    }

    public function onlineGameStatsMain(Request $request) {
        $prizeFund = 2000 + OnlinePlayerTransaction::query()->sum('raised_fund_amount');
        $raisingFunds = OnlinePlayerTransaction::query()->sum('received') - 1000 - OnlinePlayerTransaction::query()->sum('raised_fund_amount');

        $onlinePlayersCount = OnlinePlayer::query()->count();
        $onlineGamesCount = OnlinePlayerStat::query()->count();

        $onlineWonGamesCount = OnlinePlayerStat::whereWin(1)->count();
        $onlineCompleteGamesCount = OnlinePlayerStat::whereWin(1)->whereTotalWin(1180000)->count();

        $onlinePlayers = OnlinePlayer::with('gameSessions')->orderBy('total_points', 'desc')->get();

        return view('back.online.index', compact('onlinePlayersCount', 'onlineGamesCount',
            'onlineWonGamesCount', 'onlineCompleteGamesCount', 'onlinePlayers', 'prizeFund', 'raisingFunds'));
    }

    public function getSettingsPage() {
        $currentUser = Auth::user();

        return view('back.adm_settings', compact('currentUser'));
    }

    public function saveSettingsPage(Request $request) {
        $currentUser = User::find(Auth::user()->id);

        $currentUser->first_name = $request->first_name;
        $currentUser->last_name = $request->last_name;

        if (!empty($request->file('avatar'))) {
            $currentUser->avatar = Storage::disk('avatars')->putFileAs('', $request->file('avatar'), $currentUser->id . '.' . $request->file('avatar')->getClientOriginalExtension());
        }

        $currentUser->save();

        return view('back.adm_settings', compact('currentUser'));
    }
}
