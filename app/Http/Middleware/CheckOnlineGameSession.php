<?php

namespace App\Http\Middleware;

use App\OnlinePlayerSession;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Cookie;

class CheckOnlineGameSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($og_sessid = Cookie::get('online_sessid')) {
            # если получаем ID игровой сессии, то мы проверяем, подходит ли под два требования
            # ID сессии существует, а также сессия ещё жива
            $checkSession = OnlinePlayerSession::whereSessionKey($og_sessid)
                ->where('valid_through', '>', Carbon::now('Europe/Moscow')->toDateTimeString())->first();
            if ($checkSession) {
                return $next($request);
            }
        }

        return redirect()->route('online.index');
    }
}
