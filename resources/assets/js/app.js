
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.md5 = require('../../../node_modules/md5');
import Vue from 'vue';
import VueNumber from "vue-number-animation";

import 'promise-polyfill/src/polyfill'

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use(VueNumber);

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('rusrul-online-game', require('./components/RusRulGame.vue').default);

const app = new Vue({
    el: '#app'
});
