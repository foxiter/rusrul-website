@extends('layouts.main')

@section('content')
    @php $page_title = 'Страница не найдена (404)'; @endphp
    <div class="container">
        <div class="error__block">
            <div class="error__block_code">404</div>
            <div class="error__block_text">
                <h1 class="error__block_text-title">Страница не найдена</h1>
                <div class="error__block_text-description">
                    Вероятно, вы заглянули не туда, или нашли то, чего на самом деле нет.<br>
                    Вернитесь на главную, и попробуйте снова.
                </div>
            </div>
        </div>
    </div>
@endsection