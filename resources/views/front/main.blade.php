@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="news__panel">
            <h2 class="news__panel-title">{{ $newsOnHomePage->title }}</h2>
            <div class="news__panel-specs">
                <div>{{ $newsDate }}</div>
                @if ($newsAuthor = $newsOnHomePage->author)
                    <div>{{ $newsAuthor->first_name . ' ' . $newsAuthor->last_name }}</div>
                @endif
            </div>
            <div class="news__panel-text">
                {!! $newsOnHomePage->short_text !!}
            </div>
            @if ($newsOnHomePage->short_text != $newsOnHomePage->full_text)
                <a href="/news/{{ $newsOnHomePage->url }}" class="news__panel-link">Читать далее…</a>
            @endif
        </div>

        <div class="quick_actions__row">
            @if ($lastGameRelease)
            <div class="col-6 col-m-12 quick_action">
                <a href="{{ $lastGameRelease->download_link }}" target="_blank" class="quick_action__link">Скачать ПК-игру</a>
                <div class="quick_action__description">Версия: {{ $lastGameRelease->version }}, выход: {{ date('d.m.Y', strtotime($lastGameRelease->release_date)) }}</div>
                <a href="{{ route('pc.release', ['version' => $lastGameRelease->version], false) }}" class="quick_action__more">Подробнее о релизе…</a>
            </div>
            @endif
            @if ($lastEditorRelease)
            <div class="col-6 col-m-12 quick_action">
                <a href="{{ $lastEditorRelease->download_link }}" target="_blank" class="quick_action__link">Скачать редактор</a>
                <div class="quick_action__description">Версия: {{ $lastEditorRelease->version }}</div>
                <a href="{{ route('pc.editor.release', ['version' => $lastEditorRelease->version], false) }}" class="quick_action__more">Подробнее о редакторе…</a>
            </div>
            @endif
            {{--<div class="col-4 col-m-12 quick_action">
                <a href="https://forum.rusrul.ru" target="_blank" class="quick_action__link">Форум</a>
                <div class="quick_action__description">Для идей и обсуждений</div>
            </div>--}}
        </div>
    </div>
@endsection