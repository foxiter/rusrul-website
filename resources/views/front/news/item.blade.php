@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="page__inner">
            <h1 class="page__inner_title">{{ $chosenNewsItem->title }}</h1>
            <div class="news__panel-specs">
                <div>{{ convertDateToHuman($chosenNewsItem->publish_date) }}</div>
                <div>Сергей Бойцов</div>
            </div>
            <div class="page__inner_content">
                {!! $chosenNewsItem->full_text !!}
            </div>
        </div>
    </div>
@endsection