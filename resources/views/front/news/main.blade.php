@extends('layouts.main')

@section('content')
    <div class="container">
        @foreach($newsList as $newsItem)
            <div class="news__panel">
                <a class="news__panel-title" href="/news/{{ $newsItem->url }}">{{ $newsItem->title }}</a>
                <div class="news__panel-specs">
                    <div>{{ convertDateToHuman($newsItem->publish_date) }}</div>
                    <div>Сергей Бойцов</div>
                </div>
                <div class="news__panel-text">
                    {!! $newsItem->short_text !!}
                </div>
                @if ($newsItem->short_text != $newsItem->full_text)
                    <a href="/news/{{ $newsItem->url }}" class="news__panel-link">Читать далее…</a>
                @endif
            </div>
        @endforeach

        {{ $newsList->links() }}
    </div>
@endsection