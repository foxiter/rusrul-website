@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="page__inner">
            <h1 class="page__inner_title">{{ $chosenPage->title }}</h1>
            <div class="page__inner_content">
                {!! $chosenPage->page_content !!}
            </div>
        </div>
    </div>
@endsection