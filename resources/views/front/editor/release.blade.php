@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="page__inner">
            <h1 class="page__inner_title">Редактор вопросов для ПК-РусРула: версия {{ $chosenEditorRelease->version }}</h1>
            <div class="news__panel-specs">
                {{--<div>Дата релиза: {{ convertDateToHuman($chosenGameRelease->release_date) }}</div>--}}
                <div><a href="{{ $chosenEditorRelease->download_link }}" class="download__button">Скачать ({{ round($chosenEditorRelease->filesize / 1024. / 1024., 2) }} Мб)</a></div>
            </div>
            <div class="page__inner_content">
                {!! $chosenEditorRelease->description !!}
            </div>
        </div>
    </div>
@endsection