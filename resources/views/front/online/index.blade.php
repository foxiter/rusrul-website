@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="page__inner">
            <h1 class="page__inner_title">Онлайн-игра «против механизма»</h1>
            <div class="page__inner_content">
                <p>Каждому из вас предлагается испытать на себе прелести «механизма Русской Рулетки», и проверить, сможете ли вы победить его.</p>
                <p>Игра состоит из 5 раундов, в каждом из которых вам предстоит привести в действие механизм, а затем выбрать любое из шести игровых мест. После этого откроется N люков из 6-ти. Количество люков будет определено номером раунда: в первом раунде откроется 1 люк, во втором – два, и так далее.</p>
                <p>В случае, если вам повезёт, вы получите очки. За успешное прохождение первого раунда – 5 000 очков, второго – 25 тысяч, и так далее до 1 000 000 очков в качестве награды за успех в последнем, пятом раунде игры. В случае провала игра для вас закончится, а вы потеряете половину из заработанных баллов.</p>
                <p>Первые ТРИ дня игры (по 3 декабря включительно) мы НЕ будем ограничивать число попыток в день. С 4 декабря будет введено ограничение – не более 100 попыток за сутки. Таким образом мы попытаемся уравнять шансы играющих.</p>
                <p><span class="attention_highlight">ВНИМАНИЕ!</span> Соревнование будет продолжаться до 25 декабря 2021 года! То есть ваша последняя игра должна быть завершена не позднее 24 декабря 2021 года, 23:59 по московскому времени.</p>
                <p>Участник, занявший <span class="attention_highlight">первое место в общем зачёте</span>, выиграет 2 000 рублей.</p>
                <p>Участник, который <span class="attention_highlight">первым пройдёт все пять раундов</span>, получит 400 рублей.</p>
                <p>Для входа в игру авторизуйтесь через VK. Сайт собирает и хранит лишь информацию о профиле: имя, фамилию и текущий аватар, полученные посредством API социальной сети. Это необходимо для идентификации игроков. Нажимая кнопку «Войти в игру», вы соглашаетесь с условиями предоставления этих данных.</p>
                <div style="text-align: center; margin-top: 2rem">
                    @if (\Carbon\Carbon::now('Europe/Moscow')->getTimestamp() < \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', '2021-12-23 03:30:00', 'Europe/Moscow')->getTimestamp())
                        <b>Игра будет вскоре возобновлена!</b>
                    @else
                        <a class="online__app__join" href="/online/auth?social=vk&redirect_uri=/online/play">Войти в игру</a>
                    @endif
                </div>
                {{--@if (\Carbon\Carbon::now('Europe/Moscow')->getTimestamp() >= \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', '2020-05-08 00:00:00', 'Europe/Moscow')->getTimestamp())
                    @php $position = 1; @endphp
                    @foreach($topThreePlayers as $topPlayer)
                        <div class="col-12">
                            <div class="online-player">
                                <div class="online-player__position">{{ $position }}</div>
                                <div class="online-player__avatar"><img src="{{ $topPlayer->vk_avatar }}"/></div>
                                <div class="online-player__data">
                                    <div class="online-player__data-name">{{ $topPlayer->first_name . ' ' . $topPlayer->last_name }}</div>
                                    <div class="online-player__data-points">{{ number_format($topPlayer->total_points, 0, '.', ' ') }}</div>
                                    <div class="online-player__data-stats">Игр: {{ number_format($topPlayer->games_played, 0, '.', ' ') }} / Побед: {{ number_format($topPlayer->games_win, 0, '.', ' ') }} / Поражений: {{ number_format($topPlayer->games_lose, 0, '.', ' ') }}</div>
                                </div>
                                <div class="online-player__reward">
                                    @if ($position == 1)
                                        <span>+ 2 000 ₽</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @php $position++; @endphp
                    @endforeach

                    <div style="text-align: center; margin-top: 2rem">
                        Помимо этого, приз в 250 рублей получает участник, которому первым удалось пройти все 5 раундов игры:
                    </div>
                    @php $positionTopWin = 1; @endphp
                    @foreach($firstToWinTheGameTopPlayers as $firstTopWinPlayer)
                        <div class="col-12">
                            <div class="online-player">
                                <div class="online-player__position">{{ $positionTopWin }}</div>
                                <div class="online-player__avatar"><img src="{{ $firstTopWinPlayer->player->vk_avatar }}"/></div>
                                <div class="online-player__data">
                                    <div class="online-player__data-name">{{ $firstTopWinPlayer->player->first_name . ' ' . $firstTopWinPlayer->player->last_name }}</div>
                                    <div class="online-player__data-points">{{ date('d.m.Y H:i:s', strtotime($firstTopWinPlayer->created_at)) }}</div>
                                    <div class="online-player__data-stats">Игр: {{ number_format($firstTopWinPlayer->player->games_played, 0, '.', ' ') }} / Побед: {{ number_format($firstTopWinPlayer->player->games_win, 0, '.', ' ') }} / Поражений: {{ number_format($firstTopWinPlayer->player->games_lose, 0, '.', ' ') }}</div>
                                </div>
                                <div class="online-player__reward">
                                    @if ($positionTopWin == 1)
                                        <span>+ 400 ₽</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @php $positionTopWin++; @endphp
                    @endforeach
                @endif--}}
            </div>
        </div>
    </div>
@endsection