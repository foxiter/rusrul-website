@extends('layouts.main')

@section('content')
    <div class="container">
        <div id="app">
            <rusrul-online-game :session_id="'{{ $session_game }}'"></rusrul-online-game>
        </div>
    </div>
@endsection