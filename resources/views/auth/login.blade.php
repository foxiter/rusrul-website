@extends('layouts.panel')

@section('content')
    <div class="login-page">
        <div class="container d-flex align-items-center">
            <div class="form-holder has-shadow">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="info d-flex align-items-center">
                            <div class="content">
                                <div class="logo">
                                    <h1>Администрирование</h1>
                                </div>
                                <p>Вход в эту зону разрешён только пользователям, имеющим доступ на уровне администратора сайта!</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form d-flex align-items-center">
                            <div class="content">
                                <form id="login-form" method="post" action="{{ route('login') }}">
                                    {{ csrf_field() }}

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input id="login-username" type="text" name="email" value="{{ old('email') }}" required class="input-material">
                                        <label for="login-username" class="label-material{{ $errors->has('email') ? ' active' : '' }}">E-mail</label>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <input id="login-password" type="password" name="password" required="" class="input-material">
                                        <label for="login-password" class="label-material">Пароль</label>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div><button id="login" type="submit" class="btn btn-primary">Войти</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyrights text-center">
            <p>Design by <a href="https://bootstrapious.com" class="external">Bootstrapious</a>, re-designed for website by <a target="_blank" href="https://fighter-kit.ru">fighter_kit</a></p>
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
        </div>
    </div>
@endsection
