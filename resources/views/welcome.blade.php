<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('manifest.json') }}">
        <link rel="mask-icon" href="{{ asset('safari-pinned-tab.svg') }}" color="#6e0000">
        <meta name="theme-color" content="#000000">

        <title>RusRul.ru :: Come back soon!</title>

        <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}"/>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('admin.login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ route('admin.dashboard') }}">Home</a>
                    @else
                        <a href="{{ route('admin.login') }}">Login</a>
                        <a href="{{ route('admin.register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    RusRul.ru
                </div>

                <div class="m-b-md">
                    Совсем скоро вернёмся… А пока - все важные ссылочки внизу.
                </div>

                <div class="links">
                    <a href="{{ asset('/files/RR_Game_Setup.exe') }}">Скачать ПК-игру</a>
                    <a href="{{ asset('/files/RR_QEditor.exe') }}">Скачать редактор вопросов</a>
                    <a target="_blank" href="https://forum.rusrul.ru/">Форум</a>
                    <a target="_blank" href="https://vk.com/rusrul">Группа фанатов телеигры</a>
                    <a target="_blank" href="https://vk.com/rusrul_pc">Мы в ВКонтакте</a>
                </div>
            </div>
        </div>
    </body>
</html>
