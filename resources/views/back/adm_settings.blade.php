@extends('layouts.admin')

@section('content')
    <div class="page-header">
        <div class="container-fluid">
            <h2 class="h5 no-margin-bottom">Настройки</h2>
        </div>
    </div>
    <section class="no-padding-top no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="block">
                        <div class="title"><strong>Настройки профиля</strong></div>
                        <div class="block-body">
                            <form class="form-horizontal" method="POST" action="{{ route('admin.settings') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Логин</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="login" class="form-control" value="{{ $currentUser->login }}" disabled>
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Имя</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="first_name" class="form-control" value="{{ $currentUser->first_name }}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Фамилия</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="last_name" class="form-control" value="{{ $currentUser->last_name }}">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">E-mail</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="email" class="form-control" value="{{ $currentUser->email }}" disabled><small class="help-block-none">Действующий адрес электронной почты, на которую будут присылаться уведомления.</small>
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Аватар</label>
                                    <div class="col-sm-9">
                                        <input type="file" name="avatar" class="form-control"/>
                                        @if ($currentUser->avatar)
                                            <small class="help-block-none">
                                                Текущий аватар указан ниже. При заполнении поля будет загружен новый аватар с заменой существующего.
                                                <div class="avatar"><img src="/avatars/{{$currentUser->avatar}}" alt="..." class="img-fluid rounded-circle"></div>
                                            </small>
                                        @endif
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Изменить пароль</label>
                                    <div class="col-sm-9">
                                        <input type="password" name="password" class="form-control">
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Подтверждение пароля</label>
                                    <div class="col-sm-9">
                                        <input type="password" name="password_confirmation" placeholder="Введённые пароли должны совпадать!" class="form-control">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <div class="col-sm-9 ml-auto">
                                        <button type="submit" class="btn btn-primary">Сохранить изменения</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
