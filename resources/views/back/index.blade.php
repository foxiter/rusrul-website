@extends('layouts.admin')

@section('content')
    <div class="page-header">
        <div class="container-fluid">
            <h2 class="h5 no-margin-bottom">Сводка</h2>
        </div>
    </div>
    <section class="no-padding-top no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="statistic-block block">
                        <div class="progress-details d-flex align-items-end justify-content-between">
                            <div class="title">
                                <div class="icon"><i class="icon-user-1"></i></div><strong>Пользователей</strong>
                            </div>
                            <div class="number dashtext-1">1</div>
                        </div>
                        <div class="progress progress-template">
                            <div role="progressbar" style="width: 1%" aria-valuenow="1" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-template dashbg-1"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    @php $newsCount = \App\NewsArticle::whereIsPublished(1)->count(); @endphp
                    <div class="statistic-block block">
                        <div class="progress-details d-flex align-items-end justify-content-between">
                            <div class="title">
                                <div class="icon"><i class="icon-contract"></i></div><strong>Опубликованных новостей</strong>
                            </div>
                            <div class="number dashtext-2">{{ $newsCount }}</div>
                        </div>
                        <div class="progress progress-template">
                            <div role="progressbar" style="width: {{ $newsCount }}%" aria-valuenow="{{ $newsCount }}" aria-valuemin="0" aria-valuemax="100" class="progress-bar progress-bar-template dashbg-2"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    @php $pagesCount = \App\WebsitePage::count(); @endphp
                    <div class="statistic-block block">
                        <div class="progress-details d-flex align-items-end justify-content-between">
                            <div class="title">
                                <div class="icon"><i class="icon-paper-and-pencil"></i></div><strong>Страниц на сайте</strong>
                            </div>
                            <div class="number dashtext-3">{{ $pagesCount }}</div>
                        </div>
                        <div class="progress progress-template">
                            <div role="progressbar" style="width: {{ $pagesCount * 10 }}%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="10" class="progress-bar progress-bar-template dashbg-3"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="statistic-block block">
                        <div class="progress-details d-flex align-items-end justify-content-between">
                            <div class="title">
                                <div class="icon"><i class="icon-writing-whiteboard"></i></div><strong>Игроков в онлайн</strong>
                            </div>
                            <div class="number dashtext-4">{{ $activeOnlinePlayers }}</div>
                        </div>
                        <div class="progress progress-template">
                            <div role="progressbar" style="width: {{ $activeOnlinePlayers }}%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="1000" class="progress-bar progress-bar-template dashbg-4"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="messages-block block">
                        <div class="title"><strong>Последние новости</strong></div>
                        <div class="messages">
                            @foreach($lastNewsList as $lastNewsItem)
                            <a href="#" class="message d-flex align-items-center">
                                <div class="profile"><img src="/{{ $lastNewsItem->author->avatar ? 'avatars/' . $lastNewsItem->author->avatar : 'img/default_avatar.png' }}" alt="..." class="img-fluid">
                                    <div class="status online"></div>
                                </div>
                                <div class="content">   <strong class="d-block">{{ $lastNewsItem->author->login }}</strong><span class="d-block">{{ $lastNewsItem->title }}</span><small class="date d-block">{{ $lastNewsItem->publish_date }}</small></div></a>
                            @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
