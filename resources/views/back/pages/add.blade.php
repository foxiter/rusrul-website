@extends('layouts.admin')

@section('content')
    <div class="page-header">
        <div class="container-fluid">
            <h2 class="h5 no-margin-bottom">Настройки</h2>
        </div>
    </div>
    <section class="no-padding-top no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="block">
                        <div class="title"><strong>Создание новой страницы</strong></div>
                        <div class="block-body">
                            <form class="form-horizontal" method="POST" action="{{ route('admin.pages.add') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Заголовок</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="title" class="form-control" value="" placeholder="Заголовок страницы" required>
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Символьный код в URL</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="url_part" class="form-control" placeholder="Часть URL, отображаемая для открытия страницы">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label" for="f_text">Содержимое страницы:</label>
                                    <div class="col-sm-9">
                                        <textarea id="f_text" name="page_content" class="form-control wswg" required></textarea>
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <div class="col-sm-9 ml-auto">
                                        <button type="submit" class="btn btn-primary">Создать страницу</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <style>
        .bootstrap-datetimepicker-widget.dropdown-menu {
            display: block !important;
        }
    </style>
@endsection

@section('additional_scripts')
    <script>
        $(document).ready(function() {
            $('input[name="title"]').on('keyup', function() {
                $('input[name="url_part"]').val(convertToUrl(transliterate($('input[name="title"]').val())));
            });
        });
    </script>
@endsection