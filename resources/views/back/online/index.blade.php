@extends('layouts.admin')

@section('content')
    <div class="page-header">
        <div class="container-fluid">
            <h2 class="h5 no-margin-bottom">Статистика онлайн-игры</h2>
        </div>
    </div>
    <section class="no-padding-top no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2 col-sm-6">
                    <div class="statistic-block block">
                        <div class="progress-details d-flex align-items-end justify-content-between">
                            <div class="title">
                                <div class="icon"><i class="icon-user-1"></i></div><strong>Призовой фонд</strong>
                            </div>
                            <div class="number dashtext-1">{{ number_format($prizeFund, 0, '.', ' ') }} ₽</div>
                        </div>
                        <div class="progress progress-template">
                            <div role="progressbar" style="width: {{ 100 / 2500 * $prizeFund }}%" aria-valuenow="1" aria-valuemin="0" aria-valuemax="2500" class="progress-bar progress-bar-template dashbg-1"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="statistic-block block">
                        <div class="progress-details d-flex align-items-end justify-content-between">
                            <div class="title">
                                <div class="icon"><i class="icon-user-1"></i></div><strong>Игроков всего</strong>
                            </div>
                            <div class="number dashtext-1">{{ number_format($onlinePlayersCount, 0, '.', ' ') }}</div>
                        </div>
                        <div class="progress progress-template">
                            <div role="progressbar" style="width: {{ 100 / 250 * $onlinePlayersCount }}%" aria-valuenow="1" aria-valuemin="0" aria-valuemax="250" class="progress-bar progress-bar-template dashbg-1"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="statistic-block block">
                        <div class="progress-details d-flex align-items-end justify-content-between">
                            <div class="title">
                                <div class="icon"><i class="icon-contract"></i></div><strong>Сыгранных партий</strong>
                            </div>
                            <div class="number dashtext-2">{{ number_format($onlineGamesCount, 0, '.', ' ') }}</div>
                        </div>
                        <div class="progress progress-template">
                            <div role="progressbar" style="width: {{ 100 / 15000 * $onlineGamesCount }}%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="15000" class="progress-bar progress-bar-template dashbg-2"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="statistic-block block">
                        <div class="progress-details d-flex align-items-end justify-content-between">
                            <div class="title">
                                <div class="icon"><i class="icon-paper-and-pencil"></i></div><strong>Победных партий</strong>
                            </div>
                            <div class="number dashtext-3">{{ number_format($onlineWonGamesCount, 0, '.', ' ') }}</div>
                        </div>
                        <div class="progress progress-template">
                            <div role="progressbar" style="width: {{ 100 / 1500 * $onlineWonGamesCount }}%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="1500" class="progress-bar progress-bar-template dashbg-3"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="statistic-block block">
                        <div class="progress-details d-flex align-items-end justify-content-between">
                            <div class="title">
                                <div class="icon"><i class="icon-writing-whiteboard"></i></div><strong>Полных прохождений</strong>
                            </div>
                            <div class="number dashtext-4">{{ number_format($onlineCompleteGamesCount, 0, '.', ' ') }}</div>
                        </div>
                        <div class="progress progress-template">
                            <div role="progressbar" style="width: {{ 100 / 750 * $onlineCompleteGamesCount }}%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="750" class="progress-bar progress-bar-template dashbg-4"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="statistic-block block">
                        <div class="progress-details d-flex align-items-end justify-content-between">
                            <div class="title">
                                <div class="icon"><i class="icon-writing-whiteboard"></i></div><strong>Выручка</strong>
                            </div>
                            <div class="number dashtext-4">{{ number_format($raisingFunds, 2, '.', ' ') }} ₽</div>
                        </div>
                        <div class="progress progress-template">
                            <div role="progressbar" style="width: {{ 100 / 2250 * $raisingFunds }}%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="2250" class="progress-bar progress-bar-template dashbg-4"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="no-padding-top no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="block">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th class="text-center" style="width: 5%">Место</th>
                                    <th style="width: 25%">Игрок</th>
                                    <th style="width: 10%; font-family: monospace;">Вступил в игру</th>
                                    <th style="width: 10%; font-family: monospace;">Последняя игра</th>
                                    <th class="text-right" style="width: 5%">Всего игр</th>
                                    <th class="text-right" style="width: 5%">Попыток</th>
                                    <th class="text-right" style="width: 10%">Побед</th>
                                    <th class="text-right" style="width: 10%">Полных</th>
                                    <th class="text-right" style="width: 10%">Поражений</th>
                                    <th class="text-right" style="width: 10%">Сумма баллов</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $position = 1; @endphp
                                @forelse($onlinePlayers as $onlinePlayer)
                                    <tr>
                                        <th scope="row" class="text-center">{{ $position }}</th>
                                        <td>
                                            <a href="https://vk.com/{{ $onlinePlayer->vk_screen_name }}" target="_blank" class="message d-flex align-items-center">
                                                <div class="profile"><img src="{{ $onlinePlayer->vk_avatar }}" alt="..." class="img-fluid">
                                                    @if (count($onlinePlayer->gameSessions)) <div class="status online"></div> @endif
                                                </div>
                                                {{ $onlinePlayer->first_name . ' ' . $onlinePlayer->last_name }}
                                            </a>
                                        </td>
                                        <td style="font-family: monospace;">{{ date('d.m.Y H:i', strtotime($onlinePlayer->created_at)) }}</td>
                                        <td style="font-family: monospace;">{{ date('d.m.Y H:i', strtotime($onlinePlayer->updated_at)) }}</td>
                                        <td class="text-right">{{ number_format($onlinePlayer->games_played, 0, '.', ' ') }}</td>
                                        <td class="text-right">{{ number_format($onlinePlayer->game_takes, 0, '.', ' ') }}</td>
                                        <td class="text-right">{{ number_format($onlinePlayer->games_win, 0, '.', ' ') }}</td>
                                        <td class="text-right">{{ number_format($onlinePlayer->gameWins(), 0, '.', ' ') }}</td>
                                        <td class="text-right">{{ number_format($onlinePlayer->games_lose, 0, '.', ' ') }}</td>
                                        <td class="text-right">{{ number_format($onlinePlayer->total_points, 0, '.', ' ') }}</td>
                                    </tr>
                                    @php $position++; @endphp
                                @empty
                                    <tr>
                                        <td colspan="8" class="text-center">Нет ни одного игрока!</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <style>
        .profile {
            width: 24px;
            height: 24px;
            border-radius: 50%;
            margin-right: 10px;
            border: 2px solid #676767;
            box-shadow: 0 0 0 3px #3e3e3e;
            position: relative;
        }

        .profile img {
            border-radius: 50%;
            width: auto;
            position: absolute;
            top: 0;
            left: 0;
        }

        .profile .status {
            position: absolute;
            width: 5px;
            height: 5px;
            background: lime;
            right: 0;
            top: 0;
            border-radius: 50%;
        }
    </style>
@endsection
