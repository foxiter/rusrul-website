@extends('layouts.admin')

@section('content')
    <div class="page-header">
        <div class="container-fluid">
            <h2 class="h5 no-margin-bottom">Настройки</h2>
        </div>
    </div>
    <section class="no-padding-top no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="block">
                        <div class="title"><strong>Создание новой новости</strong></div>
                        <div class="block-body">
                            <form class="form-horizontal" method="POST" action="{{ route('admin.news.add') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Заголовок</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="title" class="form-control" value="" placeholder="Заголовок новости" required>
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Символьный код</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="url" class="form-control" placeholder="Часть URL, отображаемая для открытия страницы">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label" for="s_text">Краткий текст новости:</label>
                                    <div class="col-sm-9">
                                        <textarea id="s_text" name="short_text" class="form-control wswg" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label" for="f_text">Полный текст новости:</label>
                                    <div class="col-sm-9">
                                        <textarea id="f_text" name="full_text" class="form-control wswg" required></textarea>
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label for="published_date" class="col-sm-3 form-control-label">Дата публикации:</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="publish_date" id="published_date" readonly value="" class="form-control">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Настройки отображения<br><small class="text-primary">Примечание: на главной может находиться лишь одна новость одновременно!</small></label>
                                    <div class="col-sm-9">
                                        <div>
                                            <input id="option1" type="checkbox" name="is_published" value="1">
                                            <label for="option1">Новость опубликована</label>
                                        </div>
                                        <div>
                                            <input id="option2" type="checkbox" name="show_on_homepage" value="1">
                                            <label for="option2">Отображать новость на главной</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <div class="col-sm-9 ml-auto">
                                        <button type="submit" class="btn btn-primary">Добавить новость</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <style>
        .bootstrap-datetimepicker-widget.dropdown-menu {
            display: block !important;
        }
    </style>
@endsection

@section('additional_scripts')
    <script>
        $(document).ready(function() {
            $('input[name="title"]').on('keyup', function() {
                $('input[name="url"]').val(convertToUrl(transliterate($('input[name="title"]').val())));
            });
        });
    </script>
@endsection