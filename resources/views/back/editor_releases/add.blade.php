@extends('layouts.admin')

@section('content')
    <div class="page-header">
        <div class="container-fluid">
            <h2 class="h5 no-margin-bottom">Релизы редактора вопросов / Создание нового релиза</h2>
        </div>
    </div>
    <section class="no-padding-top no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="block">
                        <div class="title"><strong>Данные нового релиза</strong></div>
                        <div class="block-body">
                            <form class="form-horizontal" method="POST" action="{{ route('admin.qeditor.new_release') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Мажорная версия</label>
                                    <div class="col-sm-3">
                                        <input type="number" name="major_version" class="form-control" value="" placeholder="0" min="0" required>
                                    </div>
                                    <label class="col-sm-3 form-control-label">Минорная версия</label>
                                    <div class="col-sm-3">
                                        <input type="number" name="minor_version" class="form-control" value="" placeholder="0" min="0" required>
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Текстовый вариант версии</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="version" class="form-control" readonly placeholder="0.00">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label for="published_date" class="col-sm-3 form-control-label">Дата релиза:</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="release_date" id="published_date" readonly value="" class="form-control">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label" for="f_text">Описание релиза:</label>
                                    <div class="col-sm-9">
                                        <textarea id="f_text" name="description" class="form-control wswg" required></textarea>
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Ссылка на скачивание инсталлятора или файла:</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="download_link" class="form-control" placeholder="относительно корня сайта, к примеру: /files/Install.msi">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <div class="col-sm-9 ml-auto">
                                        <button type="submit" class="btn btn-primary">Создать релиз игры</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <style>
        .bootstrap-datetimepicker-widget.dropdown-menu {
            display: block !important;
        }
    </style>
@endsection

@section('additional_scripts')
    <script>
        $(document).ready(function() {
            $('input[name="major_version"]').on('keyup', function() {
                $('input[name="version"]').val($('input[name="major_version"]').val() + '.' + $('input[name="minor_version"]').val());
            });
            $('input[name="minor_version"]').on('keyup', function() {
                $('input[name="version"]').val($('input[name="major_version"]').val() + '.' + $('input[name="minor_version"]').val());
            });
        });
    </script>
@endsection