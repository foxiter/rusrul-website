@extends('layouts.admin')

@section('content')
    <div class="page-header">
        <div class="container-fluid">
            <a href="{{ route('admin.qeditor.new_release', [], false) }}" class="btn btn-primary btn-sm" style="float: right">Создать новый релиз</a>
            <h2 class="h5 no-margin-bottom">
                Релизы редактора вопросов
            </h2>
        </div>
    </div>
    <section class="no-padding-top no-padding-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="block">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Версия</th>
                                    <th>Дата релиза</th>
                                    <th>Ссылка на установщик / файл</th>
                                    <th>Размер</th>
                                    <th>Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @forelse($releasesList as $editorRelease)
                                        <tr>
                                            <th scope="row">{{ $editorRelease->version }}</th>
                                            <td>{{ date('d.m.Y', strtotime($editorRelease->release_date)) }}</td>
                                            <td><a target="_blank" href="{{ $editorRelease->download_link }}">Скачать</a></td>
                                            <td>{{ round($editorRelease->filesize / 1024. / 1024., 2) }} Mb</td>
                                            <td>
                                                —
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="5" class="text-center">Нет ни одного релиза редактора вопросов!</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
