<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('manifest.json') }}">
    <link rel="mask-icon" href="{{ asset('safari-pinned-tab.svg') }}" color="#6e0000">
    <meta name="theme-color" content="#000000">

    <title>{{ (isset($page_title)) ? $page_title : 'Главная страница' }} ~ {{ isset($website_name) ? $website_name : 'rusrul.ru' }}</title>

    <link rel="stylesheet" type="text/css" href="{{ mix('css/front.css') }}"/>
</head>
<body>
    <header>
        <div class="links">
            <div class="container">
                <a href="/" class="logo"><img src="/img/rr_logo.png"/></a>
                <div class="menu">
                    <a href="/news" {!! (request()->is(str_replace('/', '', 'news')) || request()->is(str_replace('/', '', 'news') . '/*')) ? "class=\"chosen\"" : '' !!}>Новости</a>
                    <a href="/pc-game" {!! (request()->is(str_replace('/', '', 'pc-game')) || request()->is(str_replace('/', '', 'pc-game') . '/*')) ? "class=\"chosen\"" : '' !!}>ПК-игра</a>
                    <a href="/online" {!! (request()->is(str_replace('/', '', 'online')) || request()->is(str_replace('/', '', 'online') . '/*')) ? "class=\"chosen\"" : '' !!}>Онлайн-игра</a>
                    <a href="/about" {!! (request()->is(str_replace('/', '', 'about')) || request()->is(str_replace('/', '', 'about') . '/*')) ? "class=\"chosen\"" : '' !!}>О проекте</a>
                </div>
                <div class="menu__mobile-toggle">
                    <span>&nbsp;</span>
                    <span>&nbsp;</span>
                    <span>&nbsp;</span>
                </div>
            </div>
        </div>
    </header>

    <section class="wrapper">
        @yield('content')
    </section>

    <footer>
        <div class="container">
            <div class="footer__item">© rusrul.ru, 2010-<?= date('Y') ?></div>
            <a target="_blank" href="https://vk.com/rusrul" class="footer__item">Группа фанатов телеигры</a>
            <a target="_blank" href="https://vk.com/rusrul_pc" class="footer__item">Мы в ВКонтакте</a>
        </div>
    </footer>

    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>
    <script>
        $('.menu__mobile-toggle').click(function() {
            $('.menu').toggleClass('opened');
            $(this).toggleClass('open');
        });
    </script>
</body>