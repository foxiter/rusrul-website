<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/adm/font.css') }}" rel="stylesheet">
    <link href="{{ asset('css/adm/style.red.css') }}" rel="stylesheet">
    <link href="{{ asset('css/adm/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/adm/summernote.css') }}" rel="stylesheet">
</head>
<body>
<header class="header">
    <nav class="navbar navbar-expand-lg">
        <div class="search-panel">
            <div class="search-inner d-flex align-items-center justify-content-center">
                <div class="close-btn">Close <i class="fa fa-close"></i></div>
                <form id="searchForm" action="#">
                    <div class="form-group">
                        <input type="search" name="search" placeholder="What are you searching for...">
                        <button type="submit" class="submit">Search</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="container-fluid d-flex align-items-center justify-content-between">
            <div class="navbar-header"><a href="index.html" class="navbar-brand">
                    <div class="brand-text brand-big visible text-uppercase"><strong class="text-primary">RR</strong><strong>Admin</strong></div>
                    <div class="brand-text brand-sm"><strong class="text-primary">R</strong><strong>R</strong></div></a>
                <button class="sidebar-toggle"><i class="icon-bars"></i></button>
            </div>
            <ul class="right-menu list-inline no-margin-bottom">
                <li class="list-inline-item"><a href="#" class="search-open nav-link"><i class="icon-magnifying-glass-browser"></i></a></li>
                <li class="list-inline-item logout">
                    <a id="logout" href="#" class="nav-link" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">Выйти <i class="icon-logout"></i></a>
                </li>
            </ul>
            <form id="logout-form" action="{{ route('logout', [], false) }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div>
    </nav>
</header>
    <div class="d-flex align-items-stretch">
        <!-- Sidebar Navigation-->
        <nav id="sidebar">
            <div class="sidebar-header d-flex align-items-center">
                @php $loggedUser = \Auth::user(); @endphp
                <div class="avatar"><img src="/{{ $loggedUser->avatar ? 'avatars/' . $loggedUser->avatar : 'img/default_avatar.png' }}" alt="..." class="img-fluid rounded-circle"></div>
                <div class="title">
                    @php
                        if (\Auth::user()->first_name || \Auth::user()->last_name) { $shownCredentials = \Auth::user()->first_name . ' ' . \Auth::user()->last_name; }
                        else { $shownCredentials = \Auth::user()->login; }
                    @endphp
                    <h1 class="h5">{{ $shownCredentials }}</h1>
                    <p>Администратор</p>
                </div>
            </div><span class="heading">Сайт</span>
            <ul class="list-unstyled">
                <li class="active"><a href="{{ route('admin.dashboard', [], false) }}"> <i class="icon-home"></i>Сводка</a></li>
                <li><a href="#news" aria-expanded="false" data-toggle="collapse"> <i class="icon-windows"></i>Новости на сайте</a>
                    <ul id="news" class="collapse list-unstyled ">
                        <li><a href="#">Список новостей</a></li>
                        <li><a href="{{ route('admin.news.add', [], false) }}">Добавить новую</a></li>
                    </ul>
                </li>
                <li><a href="#pages" aria-expanded="false" data-toggle="collapse"> <i class="icon-contract"></i>Страницы</a>
                    <ul id="pages" class="collapse list-unstyled ">
                        <li><a href="#">Список страниц</a></li>
                        <li><a href="{{ route('admin.pages.add', [], false) }}">Добавить новую</a></li>
                    </ul>
                </li>
                <li><a href="{{ route('admin.pc_game.list', [], false) }}"> <i class="icon-grid"></i>Релизы ПК-игры</a></li>
                <li><a href="{{ route('admin.qeditor.list', [], false) }}"> <i class="icon-padnote"></i>Релизы редактора</a></li>
                <li><a href="{{ route('admin.online.main', [], false) }}"> <i class="icon-chart"></i>Стата онлайн-игры</a></li>
            </ul><span class="heading">Администрирование</span>
            <ul class="list-unstyled">
                <li> <a href="{{ route('admin.settings', [], false) }}"> <i class="icon-settings"></i>Настройки</a></li>
            </ul>
        </nav>
        <div class="page-content">
            @yield('content')
            <footer class="footer">
                <div class="footer__block block no-margin-bottom">
                    <div class="container-fluid text-center">
                        <!-- Please do not remove the backlink to us unless you support us at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
                        <p class="no-margin-bottom">{{ date('Y') }} &copy; RusRul.ru. Design by <a href="https://bootstrapious.com">Bootstrapious</a> & <a href="https://fighter-kit.ru">fighter_kit</a>.</p>
                    </div>
                </div>
            </footer>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"> </script>
    <script src="{{ asset('bootstrap/js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/front.js') }}"></script>
    <script src="{{ asset('js/adm/summernote.min.js')}}"></script>
    <script src="{{ asset('js/adm/moment-with-locales.min.js')}}"></script>
    <script src="{{ asset('js/adm/bootstrap-datetimepicker.min.js')}}"></script>
    <script>
        transliterate = (
            function() {
                var
                    rus = "щ   ш  ч  ц  ю  я  ё  ж  ъ  ы  э  а б в г д е з и й к л м н о п р с т у ф х ь".split(/ +/g),
                    eng = "shh sh ch cz yu ya yo zh '' y  e' a b v g d e z i j k l m n o p r s t u f kh '".split(/ +/g)
                ;
                return function(text, engToRus) {
                    var x;
                    for(x = 0; x < rus.length; x++) {
                        text = text.split(engToRus ? eng[x] : rus[x]).join(engToRus ? rus[x] : eng[x]);
                        text = text.split(engToRus ? eng[x].toUpperCase() : rus[x].toUpperCase()).join(engToRus ? rus[x].toUpperCase() : eng[x].toUpperCase());
                    }
                    return text;
                }
            }
        )();
        convertToUrl = (
            function() {
                return function (text) {
                    text = text.replace(/[\`\'\.\«\»\"]+/g, "").toLowerCase();
                    text = text.replace(/ /g, '_');
                    return text;
                }
            }
        )();
        $(document).ready(function() {
            $('textarea').summernote();
        });
        $(function () {
            $('#published_date').datetimepicker({locale: 'ru', ignoreReadonly: true});
        });
    </script>
    @yield('additional_scripts')
</body>
</html>
